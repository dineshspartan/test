$(document).ready(function() {
	$('#offer_period_end').daterangepicker({
	  ranges: {
	        '7 Days': [moment(), moment().add('days', 6)],
	        '1 month': [moment(), moment().add('months', 1)],
	        '3 months': [moment(), moment().add('months', 3)]
	  },
      dateLimit: {'days': parseInt($('#max_offer_duration').val())},
	  startDate: moment().subtract('days', 29),
	  endDate: moment()
	},
	function(start, end) {
		$('#offer_period_start').val(start.format('DD.MM.YYYY'));
		$('#offer_period_end').val(end.format('DD.MM.YYYY'));
	});

	$.each(['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'], function(index, day) {
		initialize_offer_hour_slider(day);
	});

});

function initialize_offer_hour_slider(day) {
	var day_start = '#'+day+'_start'
	var day_end = '#'+day+'_end'
	var day_slider = '#'+day+'_slider'
	
	$(day_start).change(function() {
		$(day_slider).slider( "option", "values", [ from_hhmm_to_seconds($(day_start).val()), from_hhmm_to_seconds($(day_end).val()) ] );
	});

	$(day_end).change(function() {
		$(day_slider).slider( "option", "values", [ from_hhmm_to_seconds($(day_start).val()), from_hhmm_to_seconds($(day_end).val()) ] );
	});

	$(day_slider).slider({
		range: true,
		min: 0,
		max: 24*60 - 1,
		values: [ from_hhmm_to_seconds($(day_start).val()), from_hhmm_to_seconds($(day_end).val())],
		step:5,
		slide : function(event, ui) {
			var 	val2 = ui.values[0],
			    	val3 = ui.values[1],
				minutes0 = parseInt(val2 % 60, 10),
				hours0 = parseInt(val2 / 60 % 24, 10),
				minutes1 = parseInt(val3 % 60, 10),
				hours1 = parseInt(val3 / 60 % 24, 10);
				startTime = get_time(hours0, minutes0);
				endTime = get_time(hours1, minutes1);
			$(day_start).val(startTime);
			$(day_end).val(endTime);    
		}
	});
}

function from_hhmm_to_seconds(value) {
	if (!value) return 0;
	var parts = value.split(":");
	var result = parseInt(parts[0]) * 60;
	if (parts.length > 1) {
		result = result + parseInt(parts[1]);
	}
	return result;
}

function get_time(hours, minutes) {
	minutes = minutes + "";
	hours = hours + "";
	if (minutes.length == 1) {
		minutes = "0" + minutes;
	}
	if (hours.length == 1) {
		hours = "0" + hours;
	}
	return hours + ":" + minutes;
}

