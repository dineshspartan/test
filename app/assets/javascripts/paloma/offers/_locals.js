(function(){
  // Initializes callbacks container for the this specific scope.
  Paloma.callbacks['offers'] = {};

  // Initializes locals container for this specific scope.
  // Define a local by adding property to 'locals'.
  //
  // Example:
  // locals.localMethod = function(){};
  var locals = Paloma.locals['offers'] = {};

  
  // ~> Start local definitions here and remove this line.
  locals.setup_form = function() {
      locals.setup_toggle();
      locals.initialize_kind();
      locals.initialize_locations();
      locals.initialize_hours();
      // locals.initialize_checkin();
      $('#checkIn').click(function(){
       if($(this).prop('checked')){
       alert("Select loyalty Benefit");
       $('.checkin_loyalty_benefit').css('display:block');
       $('.checkin_loyalty_benefit').show();
       $('#offer_is_redemption_false').prop('checked',false);
       $('#offer_kind_price_discount').prop('checked',false);
       $('#offer_kind_percent_discount').prop('checked',false);
       $('#offer_kind_free_text').prop('checked',false);
       $('#visit_count').prop('checked',false);
       $('#discount_types').hide();
       } else {
       	$('.checkin_loyalty_benefit').css('display:none');
       $('.checkin_loyalty_benefit').hide();
       }
       
      });
      $('#visit_count').click(function(){
       if($(this).prop('checked')){
       alert("Select Loyalty Benefit");
       $('.vc_loyalty_benefit').css('display:block');
       $('.vc_loyalty_benefit').show();
       $('#offer_is_redemption_false').prop('checked',false);
       $('.checkin_loyalty_benefit').hide();
       $('#checkIn').prop('checked',false);
       $('#offer_kind_price_discount').prop('checked',false);
       $('#offer_kind_percent_discount').prop('checked',false);
       $('#offer_kind_free_text').prop('checked',false);
       $('#discount_types').hide();
       } else {
       	$('.vc_loyalty_benefit').css('display:none');
        $('.vc_loyalty_benefit').hide();
       }
      });
      
      $('#offer_is_redemption_false').click(function(){
      	$('#discount_types').show();
      	$('#checkIn').prop('checked',false);
      	$('#visit_count').prop('checked',false);
      	$('#offer_kind_price_discount').prop('checked',true);
      	$('.checkin_loyalty_benefit').hide();
      	$('.vc_loyalty_benefit').hide();
      });
  };

  locals.setup_toggle = function() {
      $('input[name="offer[kind]"]', 'form.offer').change(locals.initialize_kind);
      $('input[name="offer[use_all_locations]"]', 'form.offer').change(locals.initialize_locations);
      $('input[name="offer[use_location_hours]"]', 'form.offer').change(locals.initialize_hours);
      // $('input[name="offer[is_redemption]"]', 'form.offer').change(locals.initialize_checkin);
  };

  locals.initialize_kind = function() {
      var kind = $('input[name="offer[kind]"]:checked', 'form.offer').val();
      $('div.offer_toggle').each(function(index, elem) {
          var t = $(this);
          var id = t.attr('id');
          if("offer_" + kind == id) {
              t.show();
          } else {
              t.hide();
          }
      });
  };

  locals.initialize_locations = function() {
      var use_all = $('input[name="offer[use_all_locations]"]:checked', 'form.offer').val();
      if (use_all == "true") {
          $('div.offer_locations').hide();
      } else {
          $('div.offer_locations').show();
      }
  };


  locals.initialize_hours = function() {
      var use_location_hours = $('input[name="offer[use_location_hours]"]:checked', 'form.offer').val();
      if (use_location_hours == "true") {
          $('div.offer_hours').hide();
      } else {
          $('div.offer_hours').show();
      }
  };

  locals.initialize_checkin = function() {
      var is_redemption = $('input[name="offer[is_redemption]"]:checked', 'form.offer').val();
      if (is_redemption == "true") {
	  $('label.checkbox').hide();
      } else {
	  $('label.checkbox').show();
      }
  };


  // Remove this line if you don't want to inherit locals defined
  // on parent's _locals.js
  Paloma.inheritLocals({from : '/', to : 'offers'});
})();
