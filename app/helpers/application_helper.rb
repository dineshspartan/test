module ApplicationHelper

  def title(page_title)
    content_for(:title) { page_title.to_s }
  end

  def yield_or_default(section, default = '')
    content_for?(section) ? content_for(section) : default
  end

  def twitterized_type(type)
    case type
      when :alert
        "alert-block"
      when :error
        "alert-error"
      when :notice
        "alert-info"
      when :success
        "alert-success"
      else
        type.to_s
    end
  end

  def safe_current_page?(*args)
    begin
      current_page? args
    rescue
      false
    end
  end

  def static_gmap_url(width, height, locations)
    markers = locations.map.with_index{ |i, j| "markers=color:red|label:#{j + 1}|#{i.latitude},#{i.longitude}" }.join '&'
    "http://maps.googleapis.com/maps/api/staticmap?size=#{width}x#{height}&#{markers}&maptype=roadmap&sensor=false"
  end

end
