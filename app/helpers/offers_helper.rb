module OffersHelper

  def timetable_hour_range(timetable, day)
    if timetable.send(day)
      "#{timetable_hour(timetable, day, :start)} &mdash; #{timetable_hour(timetable, day, :end)}".html_safe
    else
      "&nbsp;".html_safe
    end

  end

  private

  def timetable_hour(timetable, day, start_or_end)
    value = timetable.send("#{day}_#{start_or_end}")
    return "" if value.nil?
    if value.min == 0
      value.strftime("%k")
    else
      value.strftime("%k:%M")
    end
  end

end
