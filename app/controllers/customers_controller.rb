class CustomersController < ApplicationController
  before_filter :authenticate_user! ,:except => [:register_customer,:modify_profile,:customer_location,:listing_locations]
  before_action :set_customer, only: [:show, :edit, :update, :destroy]

  # GET /customers
  def index
    @customers = Customer.all
  end

  # GET /customers/1
  def show
  end

  # GET /customers/new
  def new
    @customer = Customer.new
  end

  # GET /customers/1/edit
  def edit
  end

  # POST /customers
  def create
    @customer = Customer.new(customer_params)

    if @customer.save
      redirect_to @customer, notice: 'Customer was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /customers/1
  def update
    if @customer.update(customer_params)
      redirect_to @customer, notice: 'Customer was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /customers/1
  def destroy
    @customer.destroy
    redirect_to customers_url, notice: 'Customer was successfully destroyed.'
  end
  
  def register_customer
    @name = params[:name]
    @mob_no = params[:mob_no]
    @email = params[:email]
    @fb_link = params[:fb_link]
    @bssid = params[:bssid]
     respond_to do |format|
       if request.format == 'json' 
            unless @name.nil?
              unless @mob_no.nil?
                unless @email.nil?
                 # unless @fb_link.nil?
		    unless @bssid.nil?
                    #	if Customer.where(:bssid => @bssid).exists? 
			 #  @customer = Customer.find_by_bssid(@bssid)
                         #  @customer.update_attributes!(:name => @name, :email => @email, :fb_link => @fb_link)
                      	 #  format.json {render :json => { :success => 'true',:bssid => @customer.bssid,:message => 'bssid already exist and so user updated successfully'}}
                   # 	else
                      	   if Customer.where(:mob_no => @mob_no).exists? 
			      @customer = Customer.find_by_mob_no(@mob_no) 
                              @customer.update_attributes!(:name => @name, :email => @email, :fb_link => @fb_link,:bssid => @bssid)
                              format.json {render :json => { :success => 'true',:mob_no => @customer.mob_no,:message => 'Mobile Number already exist so user updated successfully'}}
                           else
                              @customer = Customer.new(:name => @name, :mob_no => @mob_no, :email => @email, :fb_link => @fb_link, :bssid => @bssid)
                              if @customer.save
                                 format.json {render :json => { :success => 'true',:message => 'Customer has been created successfully'}}
                              else
                                 format.json {render :json => { :success => 'false',:message => 'Customer not saved',:error => @customer.errors.full_messages}}
                              end
                           end
                     #   end
		   else
		    format.json {render :json => { :success => 'false',:message => 'bssid cannot be blank'}}
		   end
                  #else
                   # format.json {render :json => { :success => 'false',:message => 'Fb Link cannot be blank'}}
                  #end
                else
                  format.json {render :json => { :success => 'false',:message => 'Email cannot be blank'}}
                end
              else
                format.json {render :json => { :success => 'false',:message => 'Mobile Number cannot be blank'}}
              end
            else
              format.json {render :json => { :success => 'false',:message => 'Name cannot be blank'}}
            end
       end
     end       
  end

  def modify_profile
    @name =  params[:name]
    @mob_no =  params[:mob_no]
    @email = params[:email]
    @fb_link = params[:fb_link]
    @bssid = params[:bssid]
      respond_to do |format|
         if request.format == 'json' 
           unless  @bssid.blank? 
             unless  @mob_no.blank?
                   @customer = Customer.find_by_mob_no_and_bssid(@mob_no,@bssid)
                   unless @customer.nil?
                     unless @name.blank? 
                        unless @email.blank?
                       #   unless @fb_link.blank?
                            @customer.update_attributes(:email => @email, :name => @name, :fb_link => @fb_link)
                            format.json {render :json => { :success => 'true',:message => 'Customer Updated successfully'}}
                       #   else
                        #    format.json {render :json => { :success => 'false',:message => 'fb link cannot be blank'}}
                         # end
                         else
                          format.json {render :json => { :success => 'false',:message => 'Email cannot be blank'}}
                         end
                       else
                         format.json {render :json => { :success => 'false',:message => 'Name cannot be blank'}}
                       end
                   else
                     format.json {render :json => { :success => 'false',:message => 'Invalid Customer'}}
                   end
              else
                 format.json {render :json => { :success => 'false',:message => 'Mobile Number cannot be blank'}}
              end 
           else
             format.json {render :json => { :success => 'false',:message => 'bssid cannot be blank'}}
           end 
         end
      end
  end

  def customer_location
    @customer_id = params[:id]                         
    @long = params[:long]
    @lat = params[:lat]                                                   
    @customer = Customer.find_by('id=?',@customer_id)
    respond_to do |format|
       if request.format == 'json' 
         unless @customer_id.nil? && @long.nil? && @lat.nil?
          unless @customer.nil?
            @customer.update_attributes!(:longitude => @long,:latitude => @lat)
            format.json {render :json => { :success => 'true',:message => 'customer location updated successfully'}}
          else
            format.json {render :json => { :success => 'false',:message => 'Customer does not exist'}}    
          end
         else
            format.json {render :json => { :success => 'false',:message => 'Params cannot be blank'}}    
         end  
       end
     end      
  end
  
  def listing_locations  #listing long lat of customers
    respond_to do |format|
       if request.format == 'json'
         @customers = Customer.all
             a = []
           @customers.each do |c|
              b = {}
             b[:cid] = c.id
             #b[:cname] = c.name
             b[:long] = c.longitude.nil? ? "" : c.longitude
             b[:lat] = c.latitude.nil? ? "" : c.latitude
             a << b
           end
            format.json {render :json => { :success => 'true',:data => a}}   
       end
    end     
  end
 
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def customer_params
      params.require(:customer).permit(:name, :mob_no, :email, :fb_link,:bssid)
    end
end
