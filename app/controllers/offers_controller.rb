class OffersController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_offer, only: [:show, :edit, :update, :destroy, :report, :close]

  # GET /offers
  def index
    @offers = Offer.all
  end

  # GET /offers/1
  def show
  end

  # GET /offers/new
  def new
    if !current_user.admin && !@company.has_active_subscription?
      flash[:error] = "Sorry, no active subscription!"
      redirect_to :back
      return
    end
    if !current_user.admin && @company.active_offers.count >= @company.max_active_offers
      flash[:error] = "Sorry, current plan limit of active offers was reached, you have #{@company.active_offers.count} open offers."
      redirect_to :back
      return
    end
    @offer = Offer.new(:company => @company)
    @offer.is_redemption = false
    @offer.kind = :price_discount
    @offer.use_location_hours = true
    @offer.use_all_locations = true
    @offer.period_start = DateTime.now
    @offer.period_end = DateTime.now + 1.month
    @offer.build_time_table
  end

  # GET /offers/1/edit
  def edit
  end

  # POST /offers
  def create
    puts "offer_params are #{offer_params}" 
    @offer = Offer.new(offer_params)

    if @offer.save
      redirect_to dashboard_merchant_path(@offer.company), notice: 'Offer was successfully created.'
    else
      render action: 'new'
    end
  end

  def close
    if @offer.update(:period_end => DateTime.now - 1.minute)
      msg = 'Offer was successfully closed.'
    else
      msg = 'An error occurred while trying to close the offer.'
    end
    redirect_to dashboard_merchant_path(@offer.company), :notice => msg
  end

  def report
    @company = @offer.company
  end

  # PATCH/PUT /offers/1
  def update
    if @offer.update(offer_params)
      redirect_to dashboard_merchant_path(@offer.company), notice: 'Offer was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /offers/1
  def destroy
    @offer.destroy
    redirect_to offers_url, notice: 'Offer was successfully destroyed.'
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_offer
      @offer = Offer.where(:id => params[:id]).first
      redirect_to root_path, :alert => "Sorry, access denied" if !current_user.admin && @offer.company.id != @company.id
    end

    # Only allow a trusted parameter "white list" through.
    def offer_params
      params.require(:offer).permit!
    end
end
