class RatingsController < ApplicationController
  before_action :set_rating, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user! ,:except => [:rating_app]

  # GET /ratings
  def index
    @ratings = Rating.all
  end

  # GET /ratings/1
  def show
  end

  # GET /ratings/new
  def new
    @rating = Rating.new
  end

  # GET /ratings/1/edit
  def edit
  end

  # POST /ratings
  def create
    @rating = Rating.new(rating_params)

    if @rating.save
      redirect_to @rating, notice: 'Rating was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /ratings/1
  def update
    if @rating.update(rating_params)
      redirect_to @rating, notice: 'Rating was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /ratings/1
  def destroy
    @rating.destroy
    redirect_to ratings_url, notice: 'Rating was successfully destroyed.'
  end
  
  def rating_app #customer will rate app against merchant's name
    @customer_id = params[:customer_id]
    @merchant_id =  params[:merchant_id]
    @rating =  params[:rating]
    respond_to do |format|
      if request.format == 'json'
          unless @customer_id.nil?
            unless @merchant_id.nil?
              unless @rating.nil?
                  @customer = Customer.find_by(:id => @customer_id)
                  unless @customer.nil?
                    @merchant = User.find_by(:id => @merchant_id)
                      unless @merchant.nil?
                        @Rdone = Rating.find_by(:customer_id => @customer.id,:user_id => @merchant.id)                    
                        if @Rdone.nil?
                            @rate =  Rating.new(:customer_id => @customer.id,:user_id => @merchant.id,:rating => @rating)
                            @rate.save! 
                            if @rate.save
                              format.json {render :json => { :success => 'true',:message => 'Rating has been created successfully'}}
                            else
                              format.json {render :json => { :success => 'false',:message => 'Rating not saved',:error => @rate.errors.full_messages}}
                            end
                         else
                           @Rdone.update_column('rating',@rating)
                          format.json {render :json => { :success => 'false',:message => 'Rating has been updated successfully'}}
                         end
                       else
                        format.json {render :json => { :success => 'false',:message => 'Merchant ID does not exist'}}
                      end
                  else
                    format.json {render :json => { :success => 'false',:message => 'Customer ID does not exist'}}
                  end
              else
                format.json {render :json => { :success => 'false',:message => 'Rating cannot be blank'}}
              end
            else
              format.json {render :json => { :success => 'false',:message => 'Merchant cannot be blank'}}
            end
          else
            format.json {render :json => { :success => 'false',:message => 'Customer ID cannot be blank'}}
          end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rating
      @rating = Rating.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def rating_params
      params.require(:rating).permit(:customer_id, :merchant_id, :rating)
    end
end
