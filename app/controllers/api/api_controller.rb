class Api::ApiController < ActionController::Base
  protect_from_forgery with: :null_session

  before_action :restrict_access

  #api calls should not interfere with devise's trackable, etc
  before_action :skip_trackable

  respond_to :json

  rescue_from Exception, :with => :error_during_processing
  rescue_from BusinessException, :with => :handle_business_exception
  rescue_from ActiveRecord::RecordNotFound, :with => :not_found
  rescue_from ActionController::RoutingError, :with => :not_found
  #rescue_from AccessDenied, :with => :unauthorized

  def bad_route
    render :json => '', :status => 404 and return
  end

  private

  def restrict_access
    authenticate_or_request_with_http_basic do |token, ignored|
      @current_user ||= User.where(:authentication_token => token).first
    end

    @current_user ||= User.where(:authentication_token => params[:auth_token]).first

    head :unauthorized unless @current_user
  end

  def admin_only
    head :unauthorized unless @current_user.try(:admin)
  end

  def merchant_only
    head :unauthorized unless @company
  end

  def skip_trackable
    request.env['devise.skip_trackable'] = true
  end

  #def default_serializer_options
  #   {root: false}
  #end

  def unauthorized
    render :json => {:error => 'unauthorized'}.to_json, :status => 401
  end

  def not_found
    render :json => {:error => 'not_found'}.to_json, :status => 404
  end

  def error_during_processing(exception)
    Rails.logger.error exception.message
    Rails.logger.error exception.backtrace.join("\n")
    render :json => { :error => exception.message }.to_json, :status => 422
  end

  def handle_business_exception(exc)
    render :json => { :error => exc.message }.to_json, :status => 400
  end


  def set_pagination
    @limit = 100
    @offset = 0
    @limit ||= [@limit, parms[:limit]].min
    @offset ||= [@offset, params[:offset]].max
  end


end
