class Api::V1::LocationsController < Api::ApiController

  respond_to :json

  before_action :set_pagination, :only => [:index]

  #parameters:
  #  company_id,
  #  category,
  #pagination parameters:
  #  limit,
  #  offset
  def index

    locations = Location.limit(@limit).offset(@offset)

    if params[:company_id].present?
      locations = locations.where(:company_id => params[:company_id])
    end

    if params[:category].present?
      locations = locations
      .includes(:company => :category)
      .where('lower(categories.name) LIKE lower(?)', "%#{params[:category]}%")
      .references(:company => :category)
    end

    respond_with locations
  end

  def show
    render :json => Location.find(params[:id])
  end

  def near
    if params[:latitude].blank? or params[:longitude].blank? or params[:radius].blank?
      render :json => { :errors => "Missing required parameters."}, :status => :unprocessable_entity
      return
    end

    respond_with Location.near(params[:latitude], params[:longitude], params[:radius])
  end

  private


  # Only allow a trusted parameter "white list" through.
  #def deals_params
  #  params.permit(:company_id, :location_id, :category, :limit, :offset, :format)
  #end

end
