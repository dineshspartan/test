class Api::V1::CompaniesController < Api::ApiController

  respond_to :json

  before_action :set_pagination, :only => [:index]

  #parameters:
  #  phone,
  #  category,
  #pagination parameters:
  #  limit,
  #  offset
  def index

    companies = Company.limit(@limit).offset(@offset)

    if params[:phone].present?
      companies = companies.where(:phone => params[:phone])
    end

    respond_with companies
  end

  def show
    render :json => Company.find(params[:id])
  end

  private


  # Only allow a trusted parameter "white list" through.
  #def deals_params
  #  params.permit(:company_id, :location_id, :category, :limit, :offset, :format)
  #end

end
