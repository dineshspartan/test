class Api::V1::DealsController < Api::ApiController

  respond_to :json

  before_action :set_pagination, :only => [:index, :search]
  before_action :load_deal, :only => [:show, :view, :claim]

  #parameters:
  #  location_id,
  #  company_id,
  #  phone,
  #  category,
  #pagination parameters:
  #  limit,
  #  offset
  def index

    offers = Offer.open.limit(@limit).offset(@offset)

    if params[:location_id].present?
      location = Location.find(params[:location_id])
      #todo: check if nil
      offers = offers
        .includes(:locations)
        .where(:id => location.offers.pluck(:id))
    end

    if params[:company_id].present?
      offers = offers.where(:company_id => params[:company_id])
    end

    if params[:phone].present?
      companies = Company.where(:phone => params[:phone])
      company = companies.first
      if company 
        offers = offers
          .includes(:locations)
          .where('(offers.use_all_locations = true and offers.company_id = ?) or locations.id in (?)', company.id, company.locations.pluck(:id))
          .references(:locations)
      else 
        respond_with [] and return
      end
    end

    if params[:category].present?
      offers = offers
        .includes(:category)
        .where('lower(categories.name) LIKE lower(?)', "%#{params[:category]}%")
        .references(:category)
    end

    respond_with offers

  end

  def show
    render json: @offer, root: 'deal'
  end

  def claim
    raise BusinessException, "Parameter user_ref is required" unless params[:user_ref].present?
    @offer.claim! params[:user_ref]
    head :status => 200
  end

  def view
    raise BusinessException, "Parameter user_ref is required" unless params[:user_ref].present?
    @offer.view! params[:user_ref]
    head :status => 200
  end

  def search
    respond_with Offer.search(params[:query], @offset, @limit)
  end


  private

  def set_pagination
    @limit = 100
    @offset = 0
    @limit ||= [@limit, parms[:limit]].min
    @offset ||= [@offset, params[:offset]].max
  end

  def load_deal
    @offer = Offer.find(params[:id])
  end

end
