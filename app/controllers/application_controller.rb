class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  #before_filter :authenticate_user!
  before_filter :set_company

  rescue_from ActionController::RedirectBackError do |exception|
    redirect_to root_path
  end

  private

  def set_company(id=nil)
    return unless current_user
    @company = current_user.company
    @company = Company.find(params[:company_id]) if current_user.admin && params[:company_id].present?
    @company = Company.find(id) unless id.nil?

    if @company && !current_user.admin
      flash[:error] = "Sorry, access denied." if @company.id != current_user.company.id
    end
  end
end
