class Admin::CompaniesController < Admin::Controller
  before_filter :authenticate_user!
  before_action :set_company, only: [:show, :edit, :manage, :update, :destroy]

  def autocomplete_search
    companies = Company.search(params[:name], 1)
    render json: companies.collect{ |t| { value: t.name } }
  end

  # GET /companies/1/manage
  def manage
  end

  # GET /companies
  def index
    @companies = Company.all
  end

  # GET /companies/1
  def show
  end

  # GET /companies/new
  def new
    @company = Company.new
  end

  # GET /companies/1/edit
  def edit
  end

  # POST /companies
  def create
    @company = Company.new(company_params)

    if @company.save
      redirect_to manage_admin_company_url(@company), notice: 'Company was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /companies/1
  def update
    if @company.update(company_params)
      redirect_to admin_dashboard_url, notice: 'Company was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /companies/1
  def destroy
    @company.destroy
    redirect_to admin_dashboard_url, notice: 'Company was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def company_params
      params.require(:company).permit!
    end
end
