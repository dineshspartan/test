class Admin::PanelsController < Admin::Controller
  before_filter :authenticate_user!
  def dashboard
    @companies = Company.search(params[:search], params[:page])
    @stats = Stats.company_dashboard_last_five_weeks
  end

  def configuration
    @plans = Plan.all
    @admins = User.where(:admin => true)
  end

  def api

  end

end

