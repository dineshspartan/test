class Admin::Controller < ApplicationController
  before_filter :authenticate_user!
  layout 'admin'

  before_filter :ensure_admin!

  def ensure_admin!
    unless current_user.admin
      redirect_to root_path, :alert => "An error ocurred, the page you were trying to access is restricted!"
    end
  end
end
