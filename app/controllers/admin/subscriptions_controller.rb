class Admin::SubscriptionsController < Admin::Controller
  before_filter :authenticate_user!
  before_action :set_subscription, only: [:show, :edit, :update, :destroy, :close]

  # GET /admin/subscriptions
  def index
    @subscriptions = Subscription.all
  end

  # GET /admin/subscriptions/1
  def show
  end

  # GET /admin/subscriptions/new
  def new
    @subscription = Subscription.new(:company_id => params[:company_id])
  end

  # GET /admin/subscriptions/1/edit
  def edit
  end

  def close
    @subscription.close!
    redirect_to manage_admin_company_url(@subscription.company)
  end

  # POST /admin/subscriptions
  def create
    @subscription = Subscription.new(subscription_params)

    if @subscription.save
      redirect_to manage_admin_company_url(@subscription.company), notice: 'Subscription was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /admin/subscriptions/1
  def update
    if @subscription.update(subscription_params)
      redirect_to manage_admin_company_url(@subscription.company), notice: 'Subscription was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /admin/subscriptions/1
  def destroy
    @subscription.destroy
    redirect_to manage_admin_company_url(@subscription.company), notice: 'Subscription was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_subscription
      @subscription = Subscription.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def subscription_params
      params.require(:subscription).permit!
    end
end
