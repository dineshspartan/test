class Admin::UsersController < Admin::Controller
  before_filter :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  def index
    @users = User.all
  end

  # GET /users/1
  def show
  end

  # GET /users/new
  def new
    if params[:company_id].present?
      @user = User.new(:company_id => params[:company_id])
    else
      @user = User.new(:admin => true)
    end
  end

  # GET /users/1/edit
  def edit
  end

  def invite
    @user = User.find(params[:id])
    @user.invite!(current_user)
    redirect_to back_url, notice: "Invitation sent to #{@user.email}"
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if User.where(:username => @user.username).exists? || User.where(:email => @user.email).exists?
      flash[:error] = "Sorry, username/email already taken."
      render action: 'new' and return
    end

    if User.invite!(user_params)
      redirect_to back_url, notice: "User invited. Email sent to: #{@user.email}"
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      redirect_to back_url, notice: 'User was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    redirect_to back_url, notice: 'User was successfully destroyed.'
  end

  private
    def back_url
      @user.company ? manage_admin_company_url(@user.company): admin_configuration_url
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit!
    end
end
