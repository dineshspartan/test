class Admin::ChartsController < Admin::Controller
  before_filter :authenticate_user!
  def company_dashboard
    render :json => Stats.company_dashboard_last_five_weeks
  end
end
