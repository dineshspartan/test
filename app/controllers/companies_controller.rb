class CompaniesController < ApplicationController
  before_filter :authenticate_user!
  before_action do
    set_company(params[:id])
  end

  # GET /companies/1/edit
  def edit
  end

  # PATCH/PUT /companies/1
  def update
    if @company.update(company_params)
      redirect_to my_account_merchant_url(@company), notice: 'Company was successfully updated.'
    else
      render action: 'edit'
    end
  end

  private
    # Only allow a trusted parameter "white list" through.
    def company_params
      params.require(:company).permit!
    end
end
