class LoyaltiesController < ApplicationController
 before_filter :authenticate_user!,except:[:edit_loyalty_benefits, :close_loyalty_profile, :closed_loyalty, :loyalty_performance, :merchant_mac, :merchant_login]
  before_action :set_loyalty, only: [:show, :edit, :update, :destroy]

  # GET /loyalties
  def index
    @loyalties = Loyalty.all
  end

  # GET /loyalties/1
  def show
  end

  # GET /loyalties/new
  def new    
    @loyalty = Loyalty.new(:company => @company)
    @loyalty.kind = :freebies
    puts "loyalty is #{@loyalty.kind}"
  end

  # GET /loyalties/1/edit
  def edit
  end

  # POST /loyalties
  def create
    puts "loyalty params are #{loyalty_params}"
    @loyalty = Loyalty.new(loyalty_params)
     
    if @loyalty.save
      redirect_to dashboard_merchant_path(@loyalty.company), notice: 'Loyalty was successfully created.'
    else
      puts "here"
      render action: 'new'
    end
  end

  # PATCH/PUT /loyalties/1
  def update
    if @loyalty.update(loyalty_params)
      redirect_to @loyalty, notice: 'Loyalty was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /loyalties/1
  def destroy
    @loyalty.destroy
    redirect_to loyalties_url, notice: 'Loyalty was successfully destroyed.'
  end

  def edit_loyalty_benefits ## edit loyalty benefit
    respond_to do |format|
      if request.format == 'json'
        unless params[:type].nil?
          unless params[:title].nil?
            unless params[:duration].nil?
              unless params[:username].nil?
                if User.where(:username => params[:username]).exists?
                  @Eoffer = Offer.find_by('id =?', params[:offer_id])
                  unless @Eoffer.nil? #### existing offer
                    if params[:type] == 'offers'
                      unless params[:discount_type].nil?
                        if params[:discount_type] == 'price'
                          unless params[:normalP].nil?
                            unless params[:reducedP].nil?
                              @user =  User.find_by_username(params[:username])
                              @Eoffer.update_attributes!(:kind => 'price_discount',:reduced_price => params[:reducedP],:normal_price => params[:normalP],:short_title => params[:title],:description => params[:description],:use_location_hours =>'t',:period_start => Time.now.strftime('%Y-%m-%d %H:%M:%S'),:period_end => (Time.now + params[:duration].to_i.days).strftime('%Y-%m-%d %H:%M:%S'),:company_id => @user.company_id )
                              @Eoffer.update_attributes!(:percent_discount => '',:checkin => '',:checkin_loyalty_benefit => '',:lb_description => '',:visit_count =>'',:vc_loyalty_benefit => '',:visit_count_number => '')
                              format.json {render :json => { :success => 'true',:message => 'Offer has been updated successfully'}}
                            else
                              format.json {render :json => { :success => 'false',:message => 'Reduced Price cannot be blank'}}
                            end
                          else
                            format.json {render :json => { :success => 'false',:message => 'Normal Price cannot be blank'}}
                          end
                        elsif params[:discount_type] == 'percent'
                          unless params[:percentD].nil?
                            @user =  User.find_by_username(params[:username])
                            @Eoffer.update_attributes!(:kind => 'percent_discount',:percent_discount => params[:percentD],:short_title => params[:title],:description => params[:description],:use_location_hours =>'t',:period_start => Time.now.strftime('%Y-%m-%d %H:%M:%S'),:period_end => (Time.now + params[:duration].to_i.days).strftime('%Y-%m-%d %H:%M:%S'),:company_id => @user.company_id )
                            @Eoffer.update_attributes!(:reduced_price => '',:normal_price => '',:checkin => '',:checkin_loyalty_benefit => '',:lb_description => '',:visit_count =>'',:vc_loyalty_benefit => '',:visit_count_number => '')
                            format.json {render :json => { :success => 'true',:message => 'Offer has been updated successfully'}}
                          else
                            format.json {render :json => { :success => 'false',:message => 'Percent Discount cannot be blank'}}
                          end
                        elsif params[:discount_type] == 'freetext'
                          @user =  User.find_by_username(params[:username])
                          @Eoffer.update_attributes!(:kind => 'free_text',:short_title => params[:title],:description => params[:description],:use_location_hours =>'t',:period_start => Time.now.strftime('%Y-%m-%d %H:%M:%S'),:period_end => (Time.now + params[:duration].to_i.days).strftime('%Y-%m-%d %H:%M:%S'),:company_id => @user.company_id )
                          @Eoffer.update_attributes!(:reduced_price => '',:normal_price => '',:percent_discount => '',:checkin => '',:checkin_loyalty_benefit => '',:lb_description => '',:visit_count =>'',:vc_loyalty_benefit => '',:visit_count_number => '')
                            format.json {render :json => { :success => 'true',:message => 'Offer has been updated successfully'}}
                        end
                      else
                        format.json {render :json => { :success => 'false',:message => 'Discount Type cannot be blank'}}
                      end
                    elsif params[:type] == 'checkin'
                      unless params[:loyalty_benefit].nil?
                        @user =  User.find_by_username(params[:username])
                        @Eoffer.update_attributes!(:checkin => 't',:checkin_loyalty_benefit => params[:loyalty_benefit],:lb_description => params[:lb_description],:short_title => params[:title],:description => params[:description],:use_location_hours =>'t',:period_start => Time.now.strftime('%Y-%m-%d %H:%M:%S'),:period_end => (Time.now + params[:duration].to_i.days).strftime('%Y-%m-%d %H:%M:%S'),:company_id => @user.company_id )
                        format.json {render :json => { :success => 'true',:message => 'Offer has been updated successfully'}}
                      else
                        format.json {render :json => { :success => 'false',:message => 'Checkin Loyalty Benefit cannot be blank'}}
                      end
                    elsif params[:type] == 'visitcount'
                      unless params[:loyalty_benefit].nil?
                        unless params[:visit_count_number].nil?
                          @user =  User.find_by_username(params[:username])
                          @Eoffer.update_attributes!(:visit_count =>'t',:vc_loyalty_benefit => params[:loyalty_benefit],:visit_count_number => params[:visit_count_number],:short_title => params[:title],:description => params[:description],:use_location_hours =>'t',:period_start => Time.now.strftime('%Y-%m-%d %H:%M:%S'),:period_end => (Time.now + params[:duration].to_i.days).strftime('%Y-%m-%d %H:%M:%S'),:company_id => @user.company_id )
                          format.json {render :json => { :success => 'true',:message => 'Offer has been updated successfully'}}
                        else
                          format.json {render :json => { :success => 'false',:message => 'Visit Count number cannot be blank'}}
                        end
                      else
                        format.json {render :json => { :success => 'false',:message => 'Visit Count Loyalty Benefit cannot be blank'}}
                      end
                    end
                 else ## offer exist check
                  format.json {render :json => { :success => 'false',:message => 'Offer doesnot exist'}}
                end
                else
                  format.json {render :json => { :success => 'false',:message => 'Username doesnot exist'}}
                end
              else
                format.json {render :json => { :success => 'false',:message => 'Username cannot be blank'}}
              end
            else
              format.json {render :json => { :success => 'false',:message => 'Duration cannot be blank'}}
            end
          else
            format.json {render :json => { :success => 'false',:message => 'Title cannot be blank'}}
          end
        else
          format.json {render :json => { :success => 'false',:message => 'Type cannot be blank'}}
        end
      end
    end
  end
 
  def close_loyalty_profile
     respond_to do |format|
      if request.format == 'json'
        @offer_id = params[:offer_id]
        @username = params[:username]
        unless @offer_id.nil? && @username.nil?
          @offer = Offer.find_by('id= ?',@offer_id)
          @user = User.find_by('username =?',@username)
          unless @offer.nil? 
            unless @user.nil?                   
              @offer.update(:period_end => DateTime.now - 1.minute) 
              format.json {render :json => { :success => 'true',:message => 'Offer has been closed successfully'}}
            else
            format.json {render :json => { :success => 'false',:message => 'User does not exist'}}
          end    
          else
            format.json {render :json => { :success => 'false',:message => 'Offer does not exist'}}
          end
        else
            format.json {render :json => { :success => 'false',:message => 'params cannot be blank'}}  
        end
      end
    end    
  end  

  def closed_loyalty  ####loyalty history offers that are closed
    respond_to do |format|
      if request.format == 'json'
	unless params[:id].nil?
          @user = User.find_by('id =?',params[:id])
	  @offer = Offer.where('period_end <? and company_id=?',DateTime.now, @user.company_id ) unless @user.nil?
	  a = []
          unless @offer.nil?
           @offer.each do |o|
            b = {}
            b[:id] = o.id
            b[:title] = o.short_title
            b[:created_on] = o.created_at.strftime('%Y-%m-%dT%I:%M %p')
            b[:closed_on] = o.period_end.strftime('%Y-%m-%dT%I:%M %p')
            b[:viewed] = ViewedOffer.find_by('offer_id =?',o.id).nil? ? "" : ViewedOffer.where('offer_id =?',o.id).count
            b[:saved] = AvailedOffer.find_by('offer_id =?',o.id).nil? ? "" : AvailedOffer.where('offer_id =?',o.id).count
            a << b 
           end
          end
	else
          format.json {render :json => { :success => 'false',:message => "Merchant ID cannot be blank"}}  
        end 
        format.json {render :json => { :success => 'true',:data => a}}  
      end
    end    
  end
  
  def loyalty_performance  ####loyalty performance of not closed offers
    respond_to do |format|
      if request.format == 'json'
       unless params[:id].nil?
          @user = User.find_by('id =?',params[:id])
          @offer = Offer.where('period_end >? and company_id=?',DateTime.now, @user.company_id ) unless @user.nil?
       	 # @offer = Offer.where('period_end >?',DateTime.now )
         a = []
        unless @offer.nil?
          @offer.each do |o|
            b = {}
            b[:id] = o.id
            b[:title] = o.short_title
            b[:created_on] = o.created_at.strftime('%Y-%m-%dT%I:%M %p')
            b[:viewed] = ViewedOffer.find_by('offer_id =?',o.id).nil? ? "" : ViewedOffer.where('offer_id =?',o.id).count
            b[:saved] = AvailedOffer.find_by('offer_id =?',o.id).nil? ? "" : AvailedOffer.where('offer_id =?',o.id).count
            a << b 
          end
         end 
	else
           format.json {render :json => { :success => 'false',:message => "Merchant ID cannot be blank"}}    
        end
          format.json {render :json => { :success => 'true',:data => a}}            
      end
    end    
  end

  def merchant_mac  ##merchant's location's mac wifi address username and password add
      respond_to do |format|
        if request.format == 'json'
          unless params[:id].blank? || params[:mac_user].blank? || params[:mac_pass].blank?
            @user = User.find_by('id=?',params[:id])
            unless @user.nil? && @user.company.blank? 
              @location = Location.find_by('company_id=?',@user.company_id)
              unless @location.blank?
                @location.update_attributes(:mac_user => params[:mac_user],:mac_pass => params[:mac_pass])
                if @location.save
                 format.json {render :json => { :success => 'true',:message => "User mac wifi details updated"}}
                else
                  format.json {render :json => { :success => 'false',:message => @location.errors.full_messages}} 
                end
              else
                format.json {render :json => { :success => 'false',:message => "Location does not exist"}}
              end  
            else
              format.json {render :json => { :success => 'false',:message => "User does not exist"}}
            end
          else
            format.json {render :json => { :success => 'false',:message => "Some parameters are missing"}}  
          end
        end
      end  
  end
  
  def merchant_login
    respond_to do |format|
      if request.format == 'json'
        unless params[:username].blank? && params[:password].blank?
          @user = User.find_by('username =?',params[:username])
          unless @user.nil?
            if @user.valid_password?(params[:password])
              format.json {render :json => { :success => 'true',:message => "Logged in successfully",:id => @user.id}}
            else
              format.json {render :json => { :success => 'false',:message => "Invalid Login"}} 
            end
          else
            format.json {render :json => { :success => 'false',:message => "User does not exist"}}  
          end  
        else
          format.json {render :json => { :success => 'false',:message => "Some parameters are missing"}} 
        end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_loyalty
      #@loyalty = Loyalty.find(params[:id])
       @loyalty = Loyalty.where(:id => params[:id]).first
      redirect_to root_path, :alert => "Sorry, access denied" if !current_user.admin && @loyalty.company.id != @company.id
    end

    # Only allow a trusted parameter "white list" through.
    def loyalty_params
      params.require(:loyalty).permit(:kind,:company_id)
    end
end
