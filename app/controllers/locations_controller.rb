class LocationsController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_location, only: [:show, :edit, :update, :destroy]

  # GET /locations
  def index
    @locations = Location.all
  end

  # GET /locations/1
  def show
  end

  # GET /locations/new
  def new
    @location = Location.new(:company_id => params[:company_id])
    @location.build_time_table
    #pp @location.time_table
  end

  # GET /locations/1/edit
  def edit
  end

  # POST /locations
  def create
    @location = Location.new(location_params)

    if @location.save
       @location.update_column(:website , Company.find_by('id =?', @location.company_id).website)
      redirect_to my_account_merchant_url(@location.company), notice: 'Location was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /locations/1
  def update
    if @location.update(location_params)
      redirect_to my_account_merchant_url(@company), notice: 'Location was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /locations/1
  def destroy
    @location.destroy
    redirect_to my_account_merchant_url(@company), notice: 'Location was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.where(:id => params[:id]).first
      redirect_to root_path, :alert => "Sorry, access denied" if !current_user.admin && @location.company.id != @company.id
    end

    # Only allow a trusted parameter "white list" through.
    def location_params
      params.require(:location).permit!
    end
end
