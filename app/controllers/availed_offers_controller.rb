class AvailedOffersController < ApplicationController
  before_action :set_availed_offer, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user! ,:except => [:customer_availed_offer,:loyalty_profile]
  # GET /availed_offers
  def index
    @availed_offers = AvailedOffer.all
  end

  # GET /availed_offers/1
  def show
  end

  # GET /availed_offers/new
  def new
    @availed_offer = AvailedOffer.new
  end

  # GET /availed_offers/1/edit
  def edit
  end

  # POST /availed_offers
  def create
    @availed_offer = AvailedOffer.new(availed_offer_params)

    if @availed_offer.save
      redirect_to @availed_offer, notice: 'Availed offer was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /availed_offers/1
  def update
    if @availed_offer.update(availed_offer_params)
      redirect_to @availed_offer, notice: 'Availed offer was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /availed_offers/1
  def destroy
    @availed_offer.destroy
    redirect_to availed_offers_url, notice: 'Availed offer was successfully destroyed.'
  end
  
  def customer_availed_offer # offer availed by customer
    @customer_id = params[:customer_id]
    @offer_id =  params[:offer_id]
    respond_to do |format|
      if request.format == 'json'
          unless @customer_id.nil?
            unless @offer_id.nil?
              @customer = Customer.find_by(:id => @customer_id)
              unless @customer.nil?
                @offer = Offer.find_by(:id => @offer_id)
                  unless @offer.nil?
                    @Aoffer = AvailedOffer.find_by(:customer_id => @customer.id,:offer_id => @offer.id)                    
                    if @Aoffer.nil?
                        @AO =  AvailedOffer.new(:customer_id => @customer.id,:offer_id => @offer.id)
                        @AO.save! 
                        if @AO.save
                          format.json {render :json => { :success => 'true',:message => 'Availed offer has been created successfully'}}
                        else
                          format.json {render :json => { :success => 'false',:message => 'Availed offer not saved',:error => @AO.errors.full_messages}}
                        end
                     else
                      format.json {render :json => { :success => 'false',:message => 'Customer has already availed this offer'}}
                     end
                   else
                    format.json {render :json => { :success => 'false',:message => 'Offer ID does not exist'}}
                  end
              else
                format.json {render :json => { :success => 'false',:message => 'Customer ID does not exist'}}
              end
            else
              format.json {render :json => { :success => 'false',:message => 'Offer cannot be blank'}}
            end
          else
            format.json {render :json => { :success => 'false',:message => 'Customer ID cannot be blank'}}
          end
      end
    end
  end

  def loyalty_profile #listing of offers availed by customer and offer created by which merchant
    respond_to do |format|
         if request.format == 'json'
	  customer_id = params[:id]
           unless customer_id.blank?
           @Ao = AvailedOffer.where('customer_id = ?',customer_id)
	    unless @Ao.blank?
            c = []
            @Ao.each do |ao|
              d = {}
              d[:offer] = ao.customer_id.nil? ? "" : ao.offer.short_title
              d[:offerID] = ao.customer_id.nil? ? "" : ao.offer.id
              d[:mname] = ao.customer_id.nil? ? "" : User.find_by(:company_id => ao.offer.company_id).nil? ? "" : User.find_by(:company_id => ao.offer.company_id).name  
              d[:cid] = ao.customer_id.nil? ? "" : ao.customer_id
              d[:cname] = ao.customer_id.nil? ? "" : ao.customer.name
              d[:datetime] = ao.updated_at.nil? ? "" : ao.updated_at.strftime('%Y-%m-%dT%I:%M %p')
               c << d               
 #            format.json {render :json => { :success => 'true',:data => [:offer => b[:offer],:offerID => b[:offerID],:mname => b[:mname],:cid =>b[:cid],:cname => b[:cname],:datetime => b[:datetime]]}}
            end     
            format.json {render :json => { :success => 'true',:data => c}}       
	    else
              format.json {render :json => { :success => 'false',:message => "Availed Offer does not exist"}}
            end   
            else
              format.json {render :json => { :success => 'false',:message => "Customer Id cannot be blank"}}
            end 
         end
    end
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_availed_offer
      @availed_offer = AvailedOffer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def availed_offer_params
      params.require(:availed_offer).permit(:customer_id, :offer_id)
    end
end
