class MerchantsController < ApplicationController
  before_filter :authenticate_user!
  before_action do
    set_company(params[:id])
  end

  def dashboard
    @stats = { :offers => Stats.offers_dashboard(@company), :redemptions => Stats.redemptions_dashboard(@company) }
  end

  def reports
    @offers = @company.offers.open
  end

  def campaigns
    @offers = @company.offers.closed
  end

  def my_account
  end

end

