class PagesController < ApplicationController
  #skip_before_filter :authenticate_user!, :only => [:create_user_using_app], if: :json_request?
  before_filter :authenticate_user! ,:except => [:create_user_using_app,:create_company_location,:loyalty_benefits,:users_details,:user_bssid,:checkin_offer,:merchant_details,:loyalty_profile]
  def home
    if current_user.try(:admin)
      redirect_to admin_dashboard_path

    elsif current_user.company
      redirect_to dashboard_merchant_path(current_user.company)

    else
      logger.fatal "Merchant user without company: #{current_user}"
      render :status => 500
    end
  end

  def create_user_using_app
    @username = params[:username]
    @email = params[:email]
    @pwd = params[:pwd]
    @cname = params[:cname]
    @cmobile = params[:cmobile]
    @cwebsite = params[:cwebsite]
    @cfbaddress = params[:cfbaddress]
    @cbssid = params[:cbssid]
    respond_to do |format|
      if request.format == 'json'
        unless @username.nil?
          unless @email.nil?
            unless @pwd.nil?
              unless @cname.nil?
                unless @cmobile.blank?
                  unless @cwebsite.blank?
                    unless @cfbaddress.blank?
                      if User.where(:username => @username).exists?
                        format.json {render :json => { :success => 'false',:message => 'Username already exist'}}
                      else
                        if User.where(:email => @email).exists?
                          format.json {render :json => { :success => 'false',:message => 'Email already exist'}}
                        else
                          if Company.where(:name => @cname).exists?
                            @c = Company.find_by_name(@cname)
                            @user = User.new(:username => @username, :email => @email, :password => @pwd,:password_confirmation=>@pwd,:admin =>'1',:company_id => @c.id)
                            if @user.save
                              format.json {render :json => { :success => 'true',:message => 'User has been created successfully'}}
                            else
                              format.json {render :json => { :success => 'false',:message => 'user not saved',:error => @user.errors.full_messages}}
                            end
                          #format.json {render :json => { :success => 'false',:message => 'Company name already exist'}}
                          else
                           unless @cbssid.nil? 
                            @company = Company.new(:name=> @cname,:phone => @cmobile, :website => @cwebsite, :fb_address => @cfbaddress,:bssid => @cbssid)
                            if @company.save
                              @user = User.new(:username => @username, :email => @email, :password => @pwd,:password_confirmation=>@pwd,:admin =>'1',:company_id => @company.id)
                              puts "@user is #{@user.encrypted_password}"
                              if @user.save
                                format.json {render :json => { :success => 'true',:message => 'User has been created successfully'}}
                              else
                                format.json {render :json => { :success => 'false',:message => 'user not saved',:error => @user.errors.full_messages}}
                              end
                            else
                              format.json {render :json => { :success => 'false',:message => 'company not saved',:error => @company.errors.full_messages}}
                            end
			   else
                              format.json {render :json => { :success => 'false',:message => 'bssid cannot be blank'}}
                            end
                          end
                        end
                      end
                    else
                      format.json {render :json => { :success => 'false',:message => 'Company fb address cannot be blank'}}
                    end
                  else
                    format.json {render :json => { :success => 'false',:message => 'Company website cannot be blank'}}
                  end

                else
                  format.json {render :json => { :success => 'false',:message => 'Company mobile number cannot be blank'}}
                end
              else
                format.json {render :json => { :success => 'false',:message => 'Company name cannot be blank'}}
              end
            else
              format.json {render :json => { :success => 'false',:message => 'Password cannot be blank'}}
            end
          else
            format.json {render :json => { :success => 'false',:message => 'Email cannot be blank'}}
          end
        else
          format.json {render :json => { :success => 'false',:message => 'Username cannot be blank'}}
        end
      end
    end
  end

  def create_company_location
    @lname = params[:lname]
    @lwebsite = params[:lwebsite]
    @lphone = params[:lphone]
    @lwifi = params[:lmacAdr]
    @address = params[:address]
    @zipcode = params[:zipcode]
    @city = params[:city]
    @opentime = params[:opentime]
    @closetime = params[:closetime]
    @username =  params[:username]
    @lat = params[:lat]
    @log = params[:log]
      respond_to do |format|
       if request.format == 'json'
         unless @lname.nil?
           unless @username.nil?
               unless @lphone.nil?
                 unless @lwifi.nil?
                   unless @address.nil?
                     unless @zipcode.nil?
                       unless @opentime.nil?
                         unless @closetime.nil?
                           unless @lat.nil?
                             unless @log.nil?
                                 if Location.where(:macwifi_address => @lwifi).exists?
                                   format.json {render :json => { :success => 'false',:message => 'Mac Wifi Address already exist'}}
                                 else
				  if User.where(:username => @username).exists?
                                       @user = User.find_by_username(@username)
                                   @location =  Location.new(:name => @lname,:phone => @lphone,:website => @user.company.website, :street_address => @address,:macwifi_address => @lwifi,:zipcode => @zipcode, :company_id => @user.company_id,:city => @city,:latitude => @lat, :longitude => @log)
                                   @time_table = TimeTable.new(:mon_start => @opentime,:tue_start => @opentime,:wed_start => @opentime,:thu_start => @opentime,:fri_start => @opentime,:sat_start => @opentime,:sun_start => @opentime,:mon_end => @closetime,:tue_end => @closetime,:wed_end => @closetime,:thu_end => @closetime,:fri_end => @closetime,:sat_end => @closetime,:sun_end => @closetime,:mon => 't',:tue => 't',:wed => 't',:thu => 't',:fri => 't',:sat => 't',:sun => 't',:location_id => @location.id)
                                   if @location.save
                                     if @time_table.save
                                       @time_table.update_column(:location_id,@location.id)
                                       puts "location id is  #{@location.id} and #{@time_table.location_id}"
                                      format.json {render :json => { :success => 'true',:message => 'Location has been created successfully'}}
                                     else
                                       format.json {render :json => { :success => 'false',:message => 'Time not saved',:error => @time_table.errors.full_messages}}
                                     end
                                   else
                                     format.json {render :json => { :success => 'false',:message => 'Location not saved',:error => @location.errors.full_messages}}
                                   end
				else
                                   format.json {render :json => { :success => 'false',:message => 'User doesnot exist'}}
                                 end
                                end
                           else
                           format.json {render :json => { :success => 'false',:message => 'Longitude coordinates cannot be blank'}}
                         end
                           else
                           format.json {render :json => { :success => 'false',:message => 'Latitude coordinates cannot be blank'}}
                         end
                         else
                           format.json {render :json => { :success => 'false',:message => 'Closing time cannot be blank'}}
                         end
                       else
                         format.json {render :json => { :success => 'false',:message => 'Opening time cannot be blank'}}
                       end
                     else
                       format.json {render :json => { :success => 'false',:message => 'Zipcode cannot be blank'}}
                     end
                   else
                     format.json {render :json => { :success => 'false',:message => 'Address cannot be blank'}}
                   end
                 else
                   format.json {render :json => { :success => 'false',:message => 'Wifi Mac Address cannot be blank'}}
                 end
               else
                 format.json {render :json => { :success => 'false',:message => 'Phone Number cannot be blank'}}
               end
            else
              format.json {render :json => { :success => 'false',:message => 'Company Id cannot be blank'}}
            end
         else
           format.json {render :json => { :success => 'false',:message => 'Location Name cannot be blank'}}
         end
       end
      end
  end

  def loyalty_benefits
    respond_to do |format|
      if request.format == 'json'
        unless params[:type].nil?
          unless params[:title].nil?
            unless params[:duration].nil?
              unless params[:username].nil?
                if User.where(:username => params[:username]).exists?
                  if params[:type] == 'offers'
                    unless params[:discount_type].nil?
                      if params[:discount_type] == 'price'
                        unless params[:normalP].nil?
                          unless params[:reducedP].nil?
                            @user =  User.find_by_username(params[:username])
                            @offer = Offer.new(:kind => 'price_discount',:reduced_price => params[:reducedP],:normal_price => params[:normalP],:short_title => params[:title],:description => params[:description],:use_location_hours =>'t',:period_start => Time.now.strftime('%Y-%m-%d %H:%M:%S'),:period_end => (Time.now + params[:duration].to_i.days).strftime('%Y-%m-%d %H:%M:%S'),:company_id => @user.company_id )
                            if @offer.save
                              format.json {render :json => { :success => 'true',:message => 'Offer has been created successfully'}}
                            else
                              format.json {render :json => { :success => 'false',:message => 'Offer not saved',:error => @offer.errors.full_messages}}
                            end
                          else
                            format.json {render :json => { :success => 'false',:message => 'Reduced Price cannot be blank'}}
                          end
                        else
                          format.json {render :json => { :success => 'false',:message => 'Normal Price cannot be blank'}}
                        end
                      elsif params[:discount_type] == 'percent'
                        unless params[:percentD].nil?
                          @user =  User.find_by_username(params[:username])
                          @offer = Offer.new(:kind => 'percent_discount',:percent_discount => params[:percentD],:short_title => params[:title],:description => params[:description],:use_location_hours =>'t',:period_start => Time.now.strftime('%Y-%m-%d %H:%M:%S'),:period_end => (Time.now + params[:duration].to_i.days).strftime('%Y-%m-%d %H:%M:%S'),:company_id => @user.company_id )
                          if @offer.save
                            format.json {render :json => { :success => 'true',:message => 'Offer has been created successfully'}}
                          else
                            format.json {render :json => { :success => 'false',:message => 'Offer not saved',:error => @offer.errors.full_messages}}
                          end
                        else
                          format.json {render :json => { :success => 'false',:message => 'Percent Discount cannot be blank'}}
                        end
                      elsif params[:discount_type] == 'freetext'
                        @user =  User.find_by_username(params[:username])
                        @offer = Offer.new(:kind => 'free_text',:short_title => params[:title],:description => params[:description],:use_location_hours =>'t',:period_start => Time.now.strftime('%Y-%m-%d %H:%M:%S'),:period_end => (Time.now + params[:duration].to_i.days).strftime('%Y-%m-%d %H:%M:%S'),:company_id => @user.company_id )
                        if @offer.save
                          format.json {render :json => { :success => 'true',:message => 'Offer has been created successfully'}}
                        else
                          format.json {render :json => { :success => 'false',:message => 'Offer not saved',:error => @offer.errors.full_messages}}
                        end
                      end
                    else
                      format.json {render :json => { :success => 'false',:message => 'Discount Type cannot be blank'}}
                    end
                  elsif params[:type] == 'checkin'
                    unless params[:loyalty_benefit].nil?
                      @user =  User.find_by_username(params[:username])
                      @offer = Offer.new(:checkin => 't',:checkin_loyalty_benefit => params[:loyalty_benefit],:lb_description => params[:lb_description],:short_title => params[:title],:description => params[:description],:use_location_hours =>'t',:period_start => Time.now.strftime('%Y-%m-%d %H:%M:%S'),:period_end => (Time.now + params[:duration].to_i.days).strftime('%Y-%m-%d %H:%M:%S'),:company_id => @user.company_id )
                      if @offer.save
                        format.json {render :json => { :success => 'true',:message => 'Offer has been created successfully'}}
                      else
                        format.json {render :json => { :success => 'false',:message => 'Offer not saved',:error => @offer.errors.full_messages}}
                      end
                    else
                      format.json {render :json => { :success => 'false',:message => 'Checkin Loyalty Benefit cannot be blank'}}
                    end
                  elsif params[:type] == 'visitcount'
                    unless params[:loyalty_benefit].nil?
                      unless params[:visit_count_number].nil?
                        @user =  User.find_by_username(params[:username])
                        @offer = Offer.new(:visit_count =>'t',:vc_loyalty_benefit => params[:loyalty_benefit],:visit_count_number => params[:visit_count_number],:short_title => params[:title],:description => params[:description],:use_location_hours =>'t',:period_start => Time.now.strftime('%Y-%m-%d %H:%M:%S'),:period_end => (Time.now + params[:duration].to_i.days).strftime('%Y-%m-%d %H:%M:%S'),:company_id => @user.company_id )
                        if @offer.save
                          format.json {render :json => { :success => 'true',:message => 'Offer has been created successfully'}}
                        else
                          format.json {render :json => { :success => 'false',:message => 'Offer not saved',:error => @offer.errors.full_messages}}
                        end
                      else
                        format.json {render :json => { :success => 'false',:message => 'Visit Count number cannot be blank'}}
                      end
                    else
                      format.json {render :json => { :success => 'false',:message => 'Visit Count Loyalty Benefit cannot be blank'}}
                    end
                  end
                else
                  format.json {render :json => { :success => 'false',:message => 'Username doesnot exist'}}
                end
              else
                format.json {render :json => { :success => 'false',:message => 'Username cannot be blank'}}
              end
            else
              format.json {render :json => { :success => 'false',:message => 'Duration cannot be blank'}}
            end
          else
            format.json {render :json => { :success => 'false',:message => 'Title cannot be blank'}}
          end
        else
          format.json {render :json => { :success => 'false',:message => 'Type cannot be blank'}}
        end
      end
    end
  end

  def users_details #merchant's details
    respond_to do |format|
      if request.format == 'json'         
         @users = User.all
         a = []  
         c = []             
             @users.each do |u|
               b = {}
              b[:id] = u.id.nil? ? "" : u.id
              b[:username] = u.username.nil? ? "" : u.username
              b[:email] = u.email.nil? ? "" : u.email
              b[:company_id] = u.company_id.nil? ? "" : u.company_id 
              b[:cname] = u.company.nil? ? "" : u.company.name
              b[:bssid] = u.company.nil? ? "" : u.company.bssid
              b[:phone] = u.company.nil? ? "" : u.company.phone
	      b[:locations] = u.company.nil? ? [] : u.company.locations
	      b[:opentime] = u.company.nil? ? "" : u.company.locations.nil? ? "" : u.company.locations.find_by_company_id(u.company_id).nil? ? "" : u.company.locations.find_by_company_id(u.company_id).time_table.mon_start.strftime("%I:%M %p")
              b[:closetime] = u.company.nil? ? "" : u.company.locations.nil? ? "" : u.company.locations.find_by_company_id(u.company_id).nil? ? "" : u.company.locations.find_by_company_id(u.company_id).time_table.mon_end.strftime("%I:%M %p")
	      b[:rating] = Rating.find_by('user_id = ?',u.id).nil? ? 0 : (Rating.where('user_id = ?',u.id).map{|r|r.rating}.sum)/(Rating.where('user_id = ?',u.id).count)			
              #b[:opentime] = u.company.nil? ? "" : u.company.locations.blank? ? "" : u.company.locations.find_by_company_id(u.company_id).time_table.mon_start
              #b[:closetime] = u.company.nil? ? "" : u.company.locations.blank? ? "" : u.company.locations.find_by_company_id(u.company_id).time_table.mon_end
	
              b[:offers] = u.company.nil? ? [] : u.company.offers
              #unless u.company.nil?
               # unless u.company.locations.blank?
                #    u.company.locations.each do |l|
                 #     b[:name] = l.name.nil? ? "" : l.name
                  #    b[:address] = l.street_address.nil? ? "" : l.street_address
                   #   b[:city] = l.city.nil? ? "" : l.city
                    #  b[:latitude] = l.latitude.nil? ? "" : l.latitude
                     # b[:longitude] = l.longitude.nil? ? "" : l.longitude
                      #    b[:opentime] = l.time_table.mon_start.nil? ? "" : l.time_table.mon_start
                       #   b[:closetime] = l.time_table.mon_end.nil? ? "" : l.time_table.mon_end                         
                   # end
              #  end                             
             # end
              a << b
         end         
          format.json {render :json => { :success => 'true',:data => a}}
      end
    end
  end
  
  def user_bssid #merchant's bssid
    respond_to do |format|
      if request.format == 'json'  
          @locations = Location.all
           a = []
          @locations.each do |l|             
              b = {}
              b[:lbssid] = l.macwifi_address.nil? ? "" : l.macwifi_address
              b[:lmac_user] = l.mac_user.nil? ? "" : l.mac_user
              b[:lmac_pass] = l.mac_pass.nil? ? "" : l.mac_pass
              a << b
           end
          format.json {render :json => { :success => 'true',:data => a}}
      end
    end
  end

   # def user_bssid #merchant's bssid
   # respond_to do |format|
    #  if request.format == 'json'  
     #     @users = User.all
      #    a = []
       #   @users.each do |u|
        #    b = {}
         #   unless u.company.nil?
          #  b[:bssid] = u.company.nil? ? [] : u.company.bssid
          #  u.company.locations.each do |l|  
          #    b[:lbssid] = l.macwifi_address
          #  end
          #  #b[:lbssid] = u.company.nil? ? [] : u.company.locations.nil? ? [] : u.company.locations.map{|l|l.macwifi_address} 
          #  a << b
          #  end
         # end
         # format.json {render :json => { :success => 'true',:data => a}}
     # end
  #  end
 # end

  def checkin_offer  ######### send checkin offer if bssid of merchant and customer gets match
      @mbssid = params[:mbssid]
      @cbssid = params[:cbssid]
      respond_to do |format|
        if request.format == 'json'
              unless @mbssid.nil?
                  unless @cbssid.nil?
                    @company = Company.find_by_bssid(@mbssid) 
                    @location = Location.find_by_macwifi_address(@mbssid)
                    @customer = Customer.find_by_bssid(@cbssid)
                    unless @customer.blank?
                      @customer.visit_count = @customer.visit_count.to_i + 1  
                      @customer.update_column(:visit_count, @customer.visit_count)  
                       unless @location.nil?
                          a = []
                          @offer = Offer.find_by_company_id_and_checkin(@location.company_id,'t')        
	                   @vc = Offer.find_by_company_id_and_visit_count(@location.company_id,'t') 
			@event = ViewedOffer.new(:customer_id =>  @customer.id,:offer_id => @offer.id,:user_id=> User.find_by('company_id =? ',@location.company_id).id)
                         if @event.save
                          b = {}
                          b[:id] = @offer.id.nil? ? "" : @offer.id unless @offer.nil?
                          b[:title] = @offer.short_title.nil? ? "" : @offer.short_title unless @offer.nil?
                          b[:description] = @offer.description.nil? ? "" : @offer.description unless @offer.nil?
                          b[:checkin_loyalty_benefit] = @offer.checkin_loyalty_benefit.nil? ? "" : @offer.checkin_loyalty_benefit unless @offer.nil?
                          b[:lb_description] =  @offer.lb_description.nil? ? "" : @offer.lb_description
                          b[:vc_loyalty_benefit] = @vc.vc_loyalty_benefit.nil? ? "" : @vc.vc_loyalty_benefit if @customer.visit_count  ==  @vc.visit_count_number unless @vc.nil?
                          b[:visit_count_number] = @vc.visit_count_number.nil? ? "" : @vc.visit_count_number if @customer.visit_count  ==  @vc.visit_count_number unless @vc.nil?
                          b[:c_vissit_count] = @customer.visit_count.nil? ? '0' : @customer.visit_count unless @vc.nil?
                          a << b            
                          format.json {render :json => { :success => 'true',:data => a}}
			else
                              format.json {render :json => { :success => 'false',:message => 'event not saved'}}
                        end
                       else
                          format.json {render :json => { :success => 'false',:message => 'mbssid not found'}}
                       end  
                     else
                        format.json {render :json => { :success => 'false',:message => 'Customer does not exist'}}
                      end
                  else
                    format.json {render :json => { :success => 'false',:message => 'Customers bssid cannot be blank'}}
                  end
            else
              format.json {render :json => { :success => 'false',:message => 'Merchants bssid cannot be blank'}}
            end 
        end
      end
  end 

  def merchant_details  #merchant's details by id
    @id =  params[:id]
      respond_to do |format|
         if request.format == 'json'
          unless @id.nil?
            @merchant =  User.find_by(:id => @id)
               a = []
            unless @merchant.nil?
              b = {}
              b[:id] =  @merchant.id.nil? ? "" : @merchant.id
              b[:username] = @merchant.username.nil? ? "" : @merchant.username
              b[:email] = @merchant.email.nil? ? "" : @merchant.email
              b[:company_id] = @merchant.company_id.nil? ? "" : @merchant.company_id
              b[:cname] = @merchant.company.nil? ? "" : @merchant.company.name
              b[:bssid] = @merchant.company.nil? ? "" : @merchant.company.bssid
              b[:phone] = @merchant.company.nil? ? "" : @merchant.company.phone
              b[:locations] = @merchant.company.nil? ? [] : @merchant.company.locations
              b[:offers] = @merchant.company.nil? ? [] : @merchant.company.offers
              b[:opentime] = @merchant.company.nil? ? "" : @merchant.company.locations.find_by_company_id(@merchant.company_id).time_table.mon_start
              b[:closetime] = @merchant.company.nil? ? "" : @merchant.company.locations.find_by_company_id(@merchant.company_id).time_table.mon_end
              a << b
              format.json {render :json => { :success => 'true',:data => a}}
            else
              format.json {render :json => { :success => 'false',:message => 'Merchant ID does not exist'}}
            end
          else
            format.json {render :json => { :success => 'false',:message => 'Merchant ID cannot be blank'}}
          end
      end
    end
  end

  def loyalty_profile #listing of offers availed by customer and offer created by which merchant
    respond_to do |format|
         if request.format == 'json'
           @Ao = AvailedOffer.all
            a = []
            @Ao.each do |ao|
              b = {}
 #             User.find_by(:company_id => ao.offer.company_id)  
              b[:offer] = ao.customer_id.nil? ? "" : ao.offer.short_title
              b[:offerID] = ao.customer_id.nil? ? "" : ao.offer.id
		User.find_by(:company_id => ao.offer.company_id)
              b[:mname] = ao.customer_id.nil? ? "" : User.find_by(:company_id => ao.offer.company_id).name.nil? ? "" : User.find_by(:company_id => ao.offer.company_id).name  
              b[:cid] = ao.customer_id.nil? ? "" : ao.customer_id
              b[:cname] = ao.customer_id.nil? ? "" : ao.customer.name
              b[:datetime] = ao.updated_at.nil? ? "" : ao.updated_at
               a << b               
            end     
            format.json {render :json => { :success => 'true',:data => a}}       
         end
    end
  end

  private

  def json_request?
    request.format.symbol == :json
  end

end
