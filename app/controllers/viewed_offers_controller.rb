class ViewedOffersController < ApplicationController
  before_action :set_viewed_offer, only: [:show, :edit, :update, :destroy]

  # GET /viewed_offers
  def index
    @viewed_offers = ViewedOffer.all
  end

  # GET /viewed_offers/1
  def show
  end

  # GET /viewed_offers/new
  def new
    @viewed_offer = ViewedOffer.new
  end

  # GET /viewed_offers/1/edit
  def edit
  end

  # POST /viewed_offers
  def create
    @viewed_offer = ViewedOffer.new(viewed_offer_params)

    if @viewed_offer.save
      redirect_to @viewed_offer, notice: 'Viewed offer was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /viewed_offers/1
  def update
    if @viewed_offer.update(viewed_offer_params)
      redirect_to @viewed_offer, notice: 'Viewed offer was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /viewed_offers/1
  def destroy
    @viewed_offer.destroy
    redirect_to viewed_offers_url, notice: 'Viewed offer was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_viewed_offer
      @viewed_offer = ViewedOffer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def viewed_offer_params
      params.require(:viewed_offer).permit(:offer_id, :customer_id, :user_id)
    end
end
