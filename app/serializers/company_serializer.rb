class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :website, :phone, :logo_thumb_url, :logo_medium_url, :description

  def attributes
    data = super
    data
  end

  def logo_thumb_url
    object.logo.url(:thumb)
  end

  def logo_medium_url
    object.logo.url(:medium)
  end


end
