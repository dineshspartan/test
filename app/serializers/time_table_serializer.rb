class TimeTableSerializer < ActiveModel::Serializer

  def attributes
    data = super
    [:mon, :tue, :wed, :thu, :fri, :sat, :sun].each do |day|
      data[day] = object.send(day)
      if object.send(day)
        data["#{day}_start"] = object.send("#{day}_start").strftime("%H:%M")
        data["#{day}_end"] = object.send("#{day}_end").strftime("%H:%M")
      end
    end
    data
  end
end
