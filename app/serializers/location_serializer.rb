class LocationSerializer < ActiveModel::Serializer
  attributes :id, :name, :website, :phone, :street_address, :zipcode, :city, :region, :company_id, :latitude, :longitude, :logo_thumb_url, :logo_medium_url
  has_many :total_open_offers, key: :deals, root: :deals_data, embed: :ids, include: true
  has_one :time_table

  def attributes
    data = super
    data
  end

  def logo_thumb_url
    object.company.logo.url(:thumb)
  end

  def logo_medium_url
    object.company.logo.url(:medium)
  end

end
