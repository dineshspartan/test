class OfferSerializer < ActiveModel::Serializer
  attributes :id, :kind, :short_title, :description, :normal_price, :reduced_price, :percent_discount, :is_redemption, :use_all_locations, :use_location_hours, :quantity_limit, :period_start, :period_end, :image_thumb_url, :checkin, :is_onetime
  has_one :time_table
  has_many :all_locations, embed: :ids, include: true, key: :locations, root: :locations_data
  has_one :category

  def attributes
    data = super
    data
  end

  def include_image_thumb_url?
    object.image.exists?
  end

  def image_thumb_url
    object.image.url(:thumb)
  end

  def include_time_table?
    !object.use_location_hours
  end

  def include_locations?
    !object.use_all_locations
  end

  def include_percent_discount?
    object.kind == 'percent_discount'
  end

  def include_normal_price?
    object.kind == 'price_discount'
  end

  def include_reduced_price?
    object.kind == 'price_discount'
  end

end
