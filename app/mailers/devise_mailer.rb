class DeviseMailer < Devise::Mailer
  address = Rails.env == 'production'? 'support@mobilegullak.in' : 'mobilegullak-dev@milchito.com'  
  default from: address, reply_to: address
end
