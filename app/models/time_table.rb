class TimeTable < ActiveRecord::Base
  belongs_to :offer
  belongs_to :location

  after_initialize do
    [:mon, :tue, :wed, :thu, :fri, :sat, :sun].each do |day|
      self.send "#{day}=", false unless self.send "#{day}"
      self.send "#{day}_start=", '09:00' unless self.send "#{day}_start"
      self.send "#{day}_end=", '18:00' unless self.send "#{day}_end"
    end
  end


end
