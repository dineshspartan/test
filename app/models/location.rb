class Location < ActiveRecord::Base
  belongs_to :company
  has_and_belongs_to_many :offers #, -> { order('is_redemption, checkin, id') }
  has_one :time_table
  accepts_nested_attributes_for :time_table
  validates :macwifi_address,:presence => true,:length => { :minimum => 12}
  validate :coordinates_must_be_present
  validates :name, :street_address, :city, presence: true
  validates :phone,:presence => true,
               :numericality => true,
               :length => { :minimum => 10, :maximum => 11 }
  validates :zipcode,:presence => true,
               :numericality => true,
               :length => { :minimum => 6 }

  default_scope { order('locations.id asc') }

  scope :near, ->(lat, long, radius) {
    #Haversine Formula based geodistance in kilometers (constant is diameter of Earth in km)
    #http://www.codecodex.com/wiki/Calculate_distance_between_two_points_on_a_globe
    query = sanitize_sql_array([%{,(
      select *, 6371 * acos(
        cos(radians(?)) * cos(radians(locations.latitude)) * cos(radians(?) - radians(locations.longitude)) + sin(radians(?)) * sin(radians(locations.latitude))
      ) as distance from locations
    ) calculated
    }, lat, long, lat])

    select("*, calculated.distance as distance")
    .includes(:offers)
    .joins(query)
    .where('calculated.id = locations.id')
    .where('calculated.distance < ?', radius)
    .order('calculated.distance')
  }

  def total_open_offers
    offers.open + company.offers.open.where(:use_all_locations => true)
  end

  def total_offers
    offers + company.offers.where(:use_all_locations => true)
  end

  def offers_total_count
    offers.length + company.offers.where(:use_all_locations => true).length
  end

  def formatted_address
    [street_address, zipcode, city, region].select{|x| x and not x.blank? }.join(', ')
  end

  private

  def coordinates_must_be_present
    if !latitude.present? || !longitude.present?
      errors.add(:base, 'Coordinates are mandatory. Just right click in the map to add a marker.')
    end
  end

end
