class Offer < ActiveRecord::Base
  has_and_belongs_to_many :locations
  belongs_to :category
  belongs_to :company
  has_one :time_table
  has_many :events
  has_many :claims
  has_attached_file :image
  accepts_nested_attributes_for :time_table

  #validates :kind, inclusion: { in: ['price_discount', 'percent_discount', 'free_text'], message: "is invalid  (%{value})." }
  validates :short_title, :period_start, :period_end, presence: true
  validates :company, presence: true

  validates :reduced_price,
            :format => { :with => /\d+(\.\d{1,2})?/ }, :numericality => {:greater_than => 0 },
            if: Proc.new { |v| v.kind == 'price_discount' }

  validates :normal_price,
            :format => { :with => /\d+(\.\d{1,2})?/ }, :numericality => {:greater_than => 0 },
            if: Proc.new { |v| v.kind == 'price_discount' }

  validates :percent_discount,
            :format => { :with => /\d+(\.\d{1,2})?/ }, :numericality => {:greater_than => 0, :less_than => 100 },
            if: Proc.new { |v| v.kind == 'percent_discount' }

  validate :period_must_be_valid

  default_scope { order('offers.is_redemption desc, offers.checkin desc, offers.id') }

  scope :open, lambda { where('period_end >= ?', Time.now) }
  scope :closed, lambda { where('period_end < ?', Time.now) }

  def self.search(query, offset, limit)
    sql = sanitize_sql_array([%{
      select distinct on (rank, id) o.id, o.*,
        ts_rank(
          array[0.1,0.1,0.6,0.9],
          setweight(to_tsvector(o.location || ' ' || o.city || ' ' || o.address), 'C') ||
          setweight(to_tsvector(o.category || ' ' || o.company), 'B') ||
          setweight(to_tsvector(o.title || ' ' || o.desc),'A'),
          query) as rank
       from
        to_tsquery(?) query,
        (select
          o.*,
          coalesce(o.short_title,'') as title,
          coalesce(o.description,'') as desc,
          coalesce(ct.name, '') as category,
          coalesce(c.name, '') as company,
          coalesce(l.name, '') as location,
          coalesce(l.city,'') as city,
          coalesce(l.street_address, '') as address
        from
          offers o left outer join categories ct on (o.category_id = ct.id), 
		companies c, locations l
        where
          o.period_end > '#{Time.now}'
          and o.company_id = c.id
          and (o.use_all_locations = true or l.id in (select l1.id from locations l1, locations_offers lo where lo.location_id = l1.id and lo.offer_id = o.id))
          and l.company_id = c.id
       ) o
       where query @@ to_tsvector(o.title || ' ' || o.desc || ' ' || o.category || ' ' || o.company  || ' ' || o.location  || ' ' || o.city || ' ' || o.address)
       order by rank desc, id
       limit ? offset ?
        }, query, limit, offset])
    #Rails.logger.debug(sql)
    Offer.find_by_sql(sql)
  end

  def view!(user_ref)
    events.create(:user_ref => user_ref, :kind => :viewed)
  end

  def claim!(user_ref)
    raise BusinessException, "Quantity limit reached." unless is_bellow_quantity_limit?

    raise BusinessException, "Offer can be claimed only once." if (self.is_onetime? and already_claimed?(user_ref))

    raise BusinessException, "User has already claimed this deal." if claims.where("user_ref = ? AND created_at > ?", user_ref, (DateTime.current - 3.hours)).exists?
    claims.create(:user_ref => user_ref)
  end

  def all_locations
    use_all_locations ? company.locations: locations
  end

  def is_bellow_quantity_limit?
    if quantity_limit.nil? or claims.count < quantity_limit
      true
    else
      false
    end
  end

  def max_offer_duration
    if company.nil?
      0
    elsif company.has_active_subscription?
      company.subscription.plan.max_duration_days
    else
      999
    end
  end

  def already_claimed?(user_ref)
    claimed_offers = self.claims.where(:offer_id => self.id, :user_ref => user_ref)
    return true if claimed_offers.present?
    return false
  end

  private

  def period_must_be_valid
    if period_end && period_start && period_end.to_date < period_start.to_date
      errors.add(:base, "Period is invalid, end date must be > start date.")
    end

    duration = (period_end.to_date - period_start.to_date).to_i
    if duration > max_offer_duration
      errors.add(:base, "Period (#{duration} days) is larger than max duration defined by current plan: #{max_offer_duration} days")
    end
  end

end
