class Loyalty < ActiveRecord::Base
  belongs_to :company
  validates :kind, presence: true
  validates :company_id, uniqueness: true
end
