class Company < ActiveRecord::Base
  has_many :users, :dependent => :delete_all
  has_many :subscriptions, :dependent => :delete_all
  has_many :offers, :dependent => :delete_all
  has_many :locations, :dependent => :delete_all
  has_many  :loyalties#,:dependent => :delete_all
  belongs_to :category

  validates :name, presence: true

  has_attached_file :logo

  def self.search(search, page)
    search ||= ''
    paginate :per_page => 5, :page => page, :conditions => ['lower(companies.name) like ?', "%#{search.downcase}%"], :order => 'lower(companies.name)'
  end

  def subscription
    self.subscriptions.order(:created_at).last
  end

  def has_active_subscription?
    s = subscription
    s and s.is_valid?
  end

  def active_offers
    offers.open
  end

  def max_active_offers
    if has_active_subscription?
      subscription.plan.active_offers
    else
      0
    end
  end

  def last_sign_in_at
    u = self.users.order("current_sign_in_at desc").first
    u.last_sign_in_at if u
  end

end
