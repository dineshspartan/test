class Plan < ActiveRecord::Base
  validates :active_offers, :max_duration_days, numericality: { only_integer: true }
end
