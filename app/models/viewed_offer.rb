class ViewedOffer < ActiveRecord::Base
  belongs_to :offer
  belongs_to :customer
  belongs_to :user
end
