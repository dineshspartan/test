class Stats < ActiveRecord::Base
  def self.company_dashboard_last_five_weeks
    now = Time.zone.now
    start = now - 5.weeks
    start = Time.zone.local(start.year, start.month, start.day, 0, 0, 0)
    self.company_dashboard(start, now, '1 week')          
  end

  #s start timestamp, e.g: Time.zone.parse('2013-01-01 00:00:00')
  #e finish timestamp, e.g: Time.zone.parse('2013-12-31 23:59:00')
  #interval '1 week'
  def self.company_dashboard(s, e, interval)
    ts_pattern = '%Y-%m-%d %H:%M:%S.%N'
    rs = ActiveRecord::Base.connection.select_all %{
      select t::timestamp::date as i, 
      (select count(*) from companies where created_at <= t + '#{interval}') as total,
      (select count(*) from companies where created_at > t and created_at <= t + '#{interval}') as new,
      (select count(*) from companies c where exists 
        (select * from subscriptions s where c.id = s.company_id and 
          (t, t + '#{interval}') overlaps (s.created_at, s.expires_at))
          ) as active
          FROM generate_series('#{s.strftime(ts_pattern)}'::timestamp, '#{e.strftime(ts_pattern)}', '#{interval}') t          
    }
    total = {}
    new = {}
    active = {}
    rs.each do |row|
      total[row['i']] = row['total']
      new[row['i']] = row['new']
      active[row['i']] = row['active']
    end
    [
      {:name => :new, :data => new, :label => 'New Companies'},
      {:name => :active, :data => active, :label => 'Active Companies'},
      {:name => :total, :data => total, :label => 'Total Companies'}
    ]
  end


  def self.offers_dashboard(company)
    now = Time.zone.now
    today = Time.zone.local(now.year, now.month, now.day, 0, 0, 0)
    yesterday = today - 1.day
    last_7days = [today - 7.days, now]
    offers(company, [ last_7days, day_range(yesterday), day_range(today) ], false)
  end

  def self.redemptions_dashboard(company)
    now = Time.zone.now
    today = Time.zone.local(now.year, now.month, now.day, 0, 0, 0)
    yesterday = today - 1.day
    last_7days = [today - 7.days, now]
    offers(company, [ last_7days, day_range(yesterday), day_range(today) ], true)
  end

  private

  def self.day_range(date)
    [Time.zone.local(date.year, date.month, date.day, 0, 0, 0), Time.zone.local(date.year, date.month, date.day, 23, 59)]
  end

  #now = Time.zone.now
  #today = Time.zone.local(now.year, now.month, mow.day, 0, 0, 0)
  #yesterday = today - 1.days
  #last_7days = today - 7.days
  def self.offers(company, ticks, is_redemption=false)
    ts_pattern = '%Y-%m-%d %H:%M:%S.%N'
    rs = ActiveRecord::Base.connection.select_all %{
      select lower_ts::timestamp::date
          as i,

      (select count(*) from offers o 
          where o.company_id = #{company.id} and o.created_at >= lower_ts and o.created_at <= upper_ts
          and o.is_redemption = #{is_redemption})
          as total,

      (select count(*) from offers o, events e 
          where e.offer_id = o.id and o.company_id = #{company.id} 
          and o.is_redemption = #{is_redemption}
          and e.kind = 'view' and e.created_at >= lower_ts and o.created_at <= upper_ts)
          as viewed,

      (select count(*) from offers o, claims c 
          where c.offer_id = o.id and o.company_id = #{company.id} 
          and o.is_redemption = #{is_redemption}
          and c.created_at >= lower_ts and o.created_at <= upper_ts)
          as claimed

      from unnest(array[ #{ticks.collect{ |u, v| "'#{u}'::timestamp"}.join(',')} ]) lower_ts,
           unnest(array[ #{ticks.collect{ |u, v| "'#{v}'::timestamp"}.join(',')} ]) upper_ts
    }
    r = process_resultset(rs, [:total, :viewed, :claimed])
  end

  def self.process_resultset(rs, categories)
    result = {}
    categories.each { |c| result[c] = {} }
    rs.each do |row|
      categories.each do |c|
        result[c][row['i']] = row[c.to_s]
      end
    end
    result
  end

end
