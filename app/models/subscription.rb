class Subscription < ActiveRecord::Base
  belongs_to :plan
  belongs_to :company

  validates :expires_at, :plan, presence: true
  validates :plan, presence: true
  validates :company, presence: true
  validate :expiration_date_cannot_be_in_the_past

  def expiration_date_cannot_be_in_the_past
    if expires_at.present? && expires_at < Date.today
      errors.add(:expiration_date, "can't be in the past")
    end
  end

  #not sure about this yet.
  def close!
    self.expires_at = Time.now - 1.minute
    save!
  end

  def is_valid?
    self.expires_at > Time.now
  end

end

