class User < ActiveRecord::Base

  # Include default devise modules. Others available are: :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :token_authenticatable

  belongs_to :company

  before_save :ensure_authentication_token

  validates :username, presence: true
  validates :username, uniqueness: true
  validates :email, uniqueness: true
  validates :company, presence: true, if: 'admin != true'

  def name
    self.username
  end


end
