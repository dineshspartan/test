class Event < ActiveRecord::Base
  belongs_to :offer
  scope :viewed, -> { where(:kind => :viewed) }
end
