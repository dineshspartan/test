class Customer < ActiveRecord::Base
    validates :name, presence: true
    validates :mob_no,:presence => true,
                 :numericality => true,
                 :length => { :minimum => 10, :maximum => 11 }, :uniqueness => true
    validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/ },:presence => true
                   # :uniqueness => true#,:email => true
    validates :fb_link, format: { with: URI.regexp }, if: Proc.new { |a| a.fb_link.present? },:presence => true
    validates :bssid,:presence => true,:length => {:minimum => 12 },:uniqueness => true
end
