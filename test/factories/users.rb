# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    email "john@test.com"
    password "example123"
    password_confirmation "example123"
    username "john"
    after(:create) { |u| u.accept_invitation! }

    factory :admin do
      admin true
    end
  end
end
