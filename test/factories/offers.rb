# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :offer do
    kind 'price_discount'
    short_title 'Offer 1'
    normal_price 10.00
    reduced_price 8.00
    period_start Time.now - 1.day
    period_end Time.now + 1.day
    company
  end
end
