# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :location do
    name 'Location 1'
    city 'Test City'
    company
    latitude -22.9077243896073
    longitude  -43.1778788566589
  end
end
