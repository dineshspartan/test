#This support package contains modules for authenticaiting
# devise users for request specs.

module ApiRequestHelper
  def basic_auth
    @env ||= {}
    @user ||= FactoryGirl.create :admin
    @env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials @user.authentication_token, nil
  end

  def json
    @json ||= JSON.parse(response.body)
  end
end

RSpec.configure do |config|
  config.include ApiRequestHelper, :type => :request
end

