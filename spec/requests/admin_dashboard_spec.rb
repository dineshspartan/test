require 'spec_helper'

describe 'Admin Dashboard' do
  before do
    @user ||= FactoryGirl.create(:admin)
    @user.accept_invitation!
    @user.save
    sign_in_as_a_valid_user
  end

  describe "GET /dashboard" do
    it "works" do
      get admin_dashboard_path
      response.status.should eq(200)
    end
  end
end
