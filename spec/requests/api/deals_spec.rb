require 'spec_helper'

describe 'Deals Requests' do
  before do
    basic_auth
    @offer = FactoryGirl.create :offer
  end

  describe 'GET /v1/deals' do
    it 'responds with deals' do
      get api_v1_deals_path, {}, @env
      response.status.should eq(200)

      get api_v1_deals_path, {:location_id => @offer.company.locations.first}, @env
      response.status.should eq(200)

      get api_v1_deals_path, {:company_id => @offer.company}, @env
      response.status.should eq(200)

      get api_v1_deals_path, {:category => 'pub'}, @env
      response.status.should eq(200)
    end

    it 'accepts pagination parameters' do
      get api_v1_deals_path, {:limit => 100, :offset => 90}, @env
      response.status.should eq(200)
    end

    it "doesnt respond closed deals" do
      #todo
    end

  end

  describe 'GET /v1/deals/{id}' do
    it 'responds with the requested deal' do
      @deal = FactoryGirl.create :offer
      get api_v1_deal_path(@deal), {}, @env
      response.status.should eq(200)
    end
  end

  describe 'POST /v1/deals/{id}/claim' do
    it 'claims a deal' do
      @deal = FactoryGirl.create :offer
      post claim_api_v1_deal_path(@deal), {:user_ref => 'u1'}, @env
      response.status.should eq(200)
    end

    it 'refuses second claim for same user' do
      @deal = FactoryGirl.create :offer
      post claim_api_v1_deal_path(@deal), {:user_ref => 'u1'}, @env
      post claim_api_v1_deal_path(@deal), {:user_ref => 'u1'}, @env
      response.status.should eq(400)
    end

    it "validates mandatory parameters" do
      @deal = FactoryGirl.create :offer
      post claim_api_v1_deal_path(@deal), {}, @env
      response.status.should eq(400)
    end
  end

  describe 'POST /v1/deals/{id}/view' do
    it 'marks a deal as viewed' do
      @deal = FactoryGirl.create :offer
      post view_api_v1_deal_path(@deal), {:user_ref => 'u1'}, @env
      response.status.should eq(200)
    end

    it "validates mandatory parameters" do
      @deal = FactoryGirl.create :offer
      post view_api_v1_deal_path(@deal), {}, @env
      response.status.should eq(400)
    end
  end

  describe 'GET /v1/deals/search' do
    it 'searches deals based on a search string' do
      get search_api_v1_deals_path, {:query => 'coffee & helsinki'}, @env
      response.status.should eq(200)
    end
  end


end