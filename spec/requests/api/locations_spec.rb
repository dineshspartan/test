require 'spec_helper'

describe 'Locations Requests' do
  before do
    basic_auth
    @location = FactoryGirl.create :location
  end

  describe 'GET /v1/locations' do
    it 'responds with locations' do
      get api_v1_locations_path, {}, @env
      response.status.should eq(200)

      get api_v1_locations_path, {:company_id => @location.company}, @env
      response.status.should eq(200)

      get api_v1_locations_path, {:category => 'pub'}, @env
      response.status.should eq(200)
    end
  end

  describe 'GET /v1/locations/{id}' do
    it 'responds with the requested location' do
      get api_v1_location_path(@location), {}, @env
    end
  end

  describe 'GET /v1/locations/near' do
    it 'responds with near locations' do
      get near_api_v1_locations_path, {
          :latitude => 0.0,
          :longitude => 0.0,
          :radius => 6000
      }, @env
      response.status.should eq(200)
    end
  end
end