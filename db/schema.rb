# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151004092634) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "availed_offers", force: true do |t|
    t.integer  "customer_id"
    t.integer  "offer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "availed_offers", ["customer_id"], name: "index_availed_offers_on_customer_id", using: :btree
  add_index "availed_offers", ["offer_id"], name: "index_availed_offers_on_offer_id", using: :btree

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "claims", force: true do |t|
    t.integer  "offer_id",   null: false
    t.string   "user_ref",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "claims", ["offer_id"], name: "index_claims_on_offer_id", using: :btree

  create_table "companies", force: true do |t|
    t.string   "name",              null: false
    t.datetime "date_joined"
    t.string   "website"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.text     "description"
    t.string   "fb_address"
    t.string   "bssid"
    t.string   "username"
    t.string   "pwd"
    t.string   "email"
  end

  add_index "companies", ["category_id"], name: "index_companies_on_category_id", using: :btree

  create_table "customers", force: true do |t|
    t.string   "name"
    t.string   "mob_no"
    t.string   "email"
    t.string   "fb_link"
    t.string   "bssid"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "visit_count"
    t.string   "longitude"
    t.string   "latitude"
  end

  create_table "events", force: true do |t|
    t.string   "user_ref"
    t.string   "kind",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "offer_id"
  end

  add_index "events", ["kind"], name: "index_events_on_kind", using: :btree
  add_index "events", ["offer_id"], name: "index_events_on_offer_id", using: :btree

  create_table "locations", force: true do |t|
    t.string   "name"
    t.string   "website"
    t.string   "phone"
    t.string   "street_address"
    t.string   "zipcode"
    t.string   "city"
    t.string   "region"
    t.integer  "company_id",      null: false
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "macwifi_address"
  end

  add_index "locations", ["company_id"], name: "index_locations_on_company_id", using: :btree

  create_table "locations_offers", force: true do |t|
    t.integer  "offer_id",    null: false
    t.integer  "location_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "locations_offers", ["location_id", "offer_id"], name: "index_locations_offers_on_location_id_and_offer_id", using: :btree

  create_table "loyalties", force: true do |t|
    t.string   "kind"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "offers", force: true do |t|
    t.string   "kind"
    t.boolean  "is_redemption"
    t.decimal  "reduced_price"
    t.decimal  "normal_price"
    t.string   "short_title"
    t.text     "description"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.text     "terms_and_conditions"
    t.boolean  "use_location_hours"
    t.boolean  "use_all_locations"
    t.integer  "quantity_limit"
    t.datetime "period_start"
    t.datetime "period_end"
    t.integer  "category_id"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "percent_discount"
    t.boolean  "checkin",                 default: false
    t.boolean  "is_onetime",              default: false
    t.boolean  "visit_count",             default: false
    t.string   "checkin_loyalty_benefit"
    t.string   "vc_loyalty_benefit"
    t.string   "lb_description"
    t.integer  "visit_count_number"
  end

  add_index "offers", ["company_id"], name: "index_offers_on_company_id", using: :btree

  create_table "plans", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "active_offers"
    t.integer  "max_duration_days"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ratings", force: true do |t|
    t.integer  "customer_id"
    t.integer  "user_id"
    t.integer  "rating"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ratings", ["customer_id"], name: "index_ratings_on_customer_id", using: :btree
  add_index "ratings", ["user_id"], name: "index_ratings_on_user_id", using: :btree

  create_table "subscriptions", force: true do |t|
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "plan_id"
    t.integer  "company_id"
  end

  add_index "subscriptions", ["company_id"], name: "index_subscriptions_on_company_id", using: :btree
  add_index "subscriptions", ["plan_id"], name: "index_subscriptions_on_plan_id", using: :btree

  create_table "time_tables", force: true do |t|
    t.boolean "mon"
    t.time    "mon_start"
    t.time    "mon_end"
    t.boolean "tue"
    t.time    "tue_start"
    t.time    "tue_end"
    t.boolean "wed"
    t.time    "wed_start"
    t.time    "wed_end"
    t.boolean "thu"
    t.time    "thu_start"
    t.time    "thu_end"
    t.boolean "fri"
    t.time    "fri_start"
    t.time    "fri_end"
    t.boolean "sat"
    t.time    "sat_start"
    t.time    "sat_end"
    t.boolean "sun"
    t.time    "sun_start"
    t.time    "sun_end"
    t.integer "offer_id"
    t.integer "location_id"
  end

  add_index "time_tables", ["location_id"], name: "index_time_tables_on_location_id", using: :btree
  add_index "time_tables", ["offer_id"], name: "index_time_tables_on_offer_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "username",                               null: false
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "authentication_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",                  default: false, null: false
    t.integer  "company_id"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "viewed_offers", force: true do |t|
    t.integer  "offer_id"
    t.integer  "customer_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "viewed_offers", ["customer_id"], name: "index_viewed_offers_on_customer_id", using: :btree
  add_index "viewed_offers", ["offer_id"], name: "index_viewed_offers_on_offer_id", using: :btree
  add_index "viewed_offers", ["user_id"], name: "index_viewed_offers_on_user_id", using: :btree

end
