class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :name
      t.text :description
      t.integer :active_offers
      t.integer :max_duration_days

      t.timestamps
    end
  end
end
