class ChangeOfferInEvents < ActiveRecord::Migration
  def change
    remove_reference :events, :offer_id
    add_reference :events, :offer
  end
end
