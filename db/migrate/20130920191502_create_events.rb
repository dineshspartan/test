class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.references :offer_id
      t.string :user_ref
      t.string :kind, :null => false
      t.timestamps
    end
  end
end
