class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.string :mob_no
      t.string :email
      t.string :fb_link
      t.string :bssid

      t.timestamps
    end
  end
end
