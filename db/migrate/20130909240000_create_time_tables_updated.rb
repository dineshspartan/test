class CreateTimeTablesUpdated < ActiveRecord::Migration
  def change
  create_table "time_tables", force: true do |t|
    t.boolean "mon"
    t.time    "mon_start"
    t.time    "mon_end"
    t.boolean "tue"
    t.time    "tue_start"
    t.time    "tue_end"
    t.boolean "wed"
    t.time    "wed_start"
    t.time    "wed_end"
    t.boolean "thu"
    t.time    "thu_start"
    t.time    "thu_end"
    t.boolean "fri"
    t.time    "fri_start"
    t.time    "fri_end"
    t.boolean "sat"
    t.time    "sat_start"
    t.time    "sat_end"
    t.boolean "sun"
    t.time    "sun_start"
    t.time    "sun_end"
    t.references :offer, :null => true
    t.references :location, :null => true
  end
  end
end
