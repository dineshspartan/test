class AddFbAddressToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :fb_address, :string
    add_column :companies, :bssid, :string
    add_column :companies, :username, :string
    add_column :companies, :pwd, :string
    add_column :companies, :email, :string
  end
end
