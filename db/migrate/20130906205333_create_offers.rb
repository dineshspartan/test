class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string :kind
      t.boolean :is_redemption
      t.decimal :reduced_price
      t.decimal :normal_price
      t.string :short_title
      t.text :description
      t.attachment :image
      t.text :terms_and_conditions
      
      t.boolean :use_location_hours
      t.boolean :use_all_locations

      t.integer :quantity_limit
      t.datetime :period_start
      t.datetime :period_end

      t.references :category
      t.references :company
    end
  end
end
