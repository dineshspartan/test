class AddIsOnetimeColumnToOffersTable < ActiveRecord::Migration
  def change
  	add_column :offers, :is_onetime, :boolean, :default => false
  end
end
