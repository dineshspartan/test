class AddCheckinToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :checkin, :boolean, :default => false
  end
end
