class AddVisitCountToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :visit_count, :integer
  end
end
