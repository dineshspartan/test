class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.datetime :expires_at
      t.timestamps
    end
    add_reference :subscriptions, :plan, :index => true
  end
end
