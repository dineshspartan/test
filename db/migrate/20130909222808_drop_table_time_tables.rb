class DropTableTimeTables < ActiveRecord::Migration
  def up
    drop_table :time_tables
  end
end
