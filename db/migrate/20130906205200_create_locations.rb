class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.string :website
      t.string :phone
      t.string :street_address
      t.string :zipcode
      t.string :city
      t.string :region
      t.references :company, :null => false
    end
  end
end
