class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name, :null => false
      t.datetime :date_joined
      t.string :website
      t.string :phone
      t.string :description
      t.timestamps
    end
    add_reference :companies, :subscription, :index => true
  end
end
