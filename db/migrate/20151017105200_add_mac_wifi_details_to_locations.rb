class AddMacWifiDetailsToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :mac_user, :string
    add_column :locations, :mac_pass, :string
  end
end
