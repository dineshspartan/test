class CreateLocationsOffers < ActiveRecord::Migration
  def change
    create_table :locations_offers do |t|
      t.references :offer, :null => false
      t.references :location, :null => false
      t.timestamps
    end
  end
end
