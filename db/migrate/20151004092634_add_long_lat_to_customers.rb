class AddLongLatToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :longitude, :string
    add_column :customers, :latitude, :string
  end
end
