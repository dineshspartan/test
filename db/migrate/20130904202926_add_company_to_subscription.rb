class AddCompanyToSubscription < ActiveRecord::Migration
  def change
    remove_column :companies, :subscription_id
    add_reference :subscriptions, :company, :index => true
  end
end
