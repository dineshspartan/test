class AddTimestampsToOffers < ActiveRecord::Migration
  def change
    add_timestamps :offers
  end
end
