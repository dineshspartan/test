class CreateViewedOffers < ActiveRecord::Migration
  def change
    create_table :viewed_offers do |t|
      t.references :offer, index: true
      t.references :customer, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
