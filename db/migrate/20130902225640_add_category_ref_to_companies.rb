class AddCategoryRefToCompanies < ActiveRecord::Migration
  def change
    add_reference :companies, :category, index: true
  end
end
