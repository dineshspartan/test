class CreateClaims < ActiveRecord::Migration
  def change
    create_table :claims do |t|
      t.references :offer, :null => false
      t.string :user_ref, :null => false
      t.timestamps
    end
  end
end
