class AddTimestampsToLocations < ActiveRecord::Migration
  def change
    add_timestamps(:locations)
  end
end
