class AddIndexes < ActiveRecord::Migration
  def change
    add_index :time_tables, :location_id
    add_index :time_tables, :offer_id
    add_index :locations, :company_id
    add_index :claims, :offer_id
    add_index :events, :offer_id
    add_index :events, :kind
    add_index :locations_offers, [:location_id, :offer_id]
    add_index :offers, :company_id
  end
end
