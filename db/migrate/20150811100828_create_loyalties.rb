class CreateLoyalties < ActiveRecord::Migration
  def change
    create_table :loyalties do |t|
      t.string :kind
      t.references :company

      t.timestamps
    end
  end
end
