class CreateAvailedOffers < ActiveRecord::Migration
  def change
    create_table :availed_offers do |t|
      t.references :customer, index: true
      t.references :offer, index: true

      t.timestamps
    end
  end
end
