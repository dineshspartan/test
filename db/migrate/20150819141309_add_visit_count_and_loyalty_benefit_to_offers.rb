class AddVisitCountAndLoyaltyBenefitToOffers < ActiveRecord::Migration
  def change
    add_column :offers, :visit_count, :boolean, :default => false
    add_column :offers, :checkin_loyalty_benefit, :string
    add_column :offers, :vc_loyalty_benefit, :string
    add_column :offers, :lb_description, :string
    add_column :offers, :visit_count_number,:integer
  end
end
