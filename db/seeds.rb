# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#


if Rails.env != 'production'
  ['cervantes', 'camus', 'alighieri', 'thomas_mann', 'poe', 'hemingway', 'kafka'].each do |u|
    User.where(:username => u).first_or_create!(
        {:email => "#{u}@milchito.com", :password => 'password', :password_confirmation => 'password', :admin => true})
  end

  companies = [
      'Acme, inc.',
      'Widget Corp',
      '123 Warehousing',
      'Smith and Co.',
      'Foo Bars',
      'Demo, inc.',
      'Luthor Corp',
      'Mammoth Pictures',
      'Duff Brewing Company',
      'The New Firm',
      'Tip Top Cafe',
      'Wenham Hogg']

  companies.each do |c|
    Company.where(:name => c).first_or_create!
  end
end


Plan.where(:name => '30 day free trial').first_or_create!({
  :description => 'Admin can give free trial for merchants in order to make sales easier.',
  :active_offers => 2,
  :max_duration_days => 90})

Plan.where(:name => 'Basic plan').first_or_create!({
  :description => 'Admin can create different sales plans for merchants. Admin can limit the amounts of active simultaneous offers and their duration for instance.',
  :active_offers => 3,
  :max_duration_days => 90})


['Pubs & Clubs', 'Sports', 'Beauty', 'Other'].each do |c|
  Category.where(:name => c).first_or_create!
end

