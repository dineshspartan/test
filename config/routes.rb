MobileGullakApp::Application.routes.draw do

resources :viewed_offers

 resources :ratings do
    collection do
      get :rating_app
    end
  end

  resources :availed_offers do 
    collection do
      get :customer_availed_offer,:loyalty_profile
    end
  end

  resources :customers do
    collection do
      get :register_customer, :modify_profile,:customer_location,:listing_locations
    end
  end

  get "pages/create_user_using_app","pages/create_company_location", "pages/loyalty_benefits","pages/users_details","pages/user_bssid","pages/checkin_offer","pages/merchant_details","pages/loyalty_profile"
  get "loyalties/edit_loyalty_benefits","loyalties/close_loyalty_profile","loyalties/closed_loyalty","loyalties/loyalty_performance","loyalties/merchant_mac","loyalties/merchant_login"

  devise_for :users

  #mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'

  namespace :admin do

    resources :companies do
      member do
        get :manage
      end
      collection do
        get :autocomplete_search
      end
    end

    resources :plans
    resources :users do
      member do
        post :invite
      end
    end
    resources :subscriptions do
      member do
        put :close
      end
    end

    get :dashboard, to: 'panels#dashboard'
    get :configuration, to: 'panels#configuration'
    get :api, to: 'panels#api'

    root :to => 'panels#dashboard'
  end

  resources :merchants, :only => [] do
    member do
      get :dashboard
      get :reports
      get :campaigns
      get :my_account
    end
  end

  resources :companies, :path => :merchants, :only => [] do
    resources :loyalties, :shallow => true
    resources :offers, :shallow => true do
      member do
        put :close
        get :report        
      end
    end
  end

  resources :companies, :only => [:edit, :update] do
    resources :locations
  end

  root 'pages#home'

  namespace :api do
    namespace :v1, :defaults => {:format => 'json'} do
      resources :deals , only: [:index, :show] do
        member do
          post :view
          post :claim
        end
        collection do
          get :search
        end
      end
      resources :locations , only: [:index, :show] do
        collection do
          get :near
        end
      end
      resources :companies, only: [:index] do
      end
    end
    get "*path" => 'api#bad_route'
  end

end
