source_folder = "/media/apps/mobilegullak"
worker_processes 6
working_directory source_folder

# This loads the application in the master process before forking
# worker processes
# Read more about it here:
# http://unicorn.bogomips.org/Unicorn/Configurator.html
preload_app true

timeout 60

# This is where we specify the socket.
# We will point the upstream Nginx module to this socket later on
listen "#{source_folder}/tmp/sockets/unicorn.sock", :backlog => 64

# not the right spot but have to be root otherwise, and that messes
# up rvm
pid "#{source_folder}/unicorn.pid"

# Set the path of the log files inside the log folder of the testapp
stderr_path "#{source_folder}/log/unicorn.stderr.log"
stdout_path "#{source_folder}/log/unicorn.stdout.log"

before_fork do |server, worker|
  # the following is highly recomended for Rails + "preload_app true"
  # as there's no need for the master process to hold a connection
  defined?(ActiveRecord::Base) and ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|
  # the following is *required* for Rails + "preload_app true",
  defined?(ActiveRecord::Base) and ActiveRecord::Base.establish_connection
end
